<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name', 'JPGS') }}</title>
    </head>
<body>
	<div class="wrapper">
		<div class="main">
			<main class="content">
				<div class="container-fluid p-0">
					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-body m-sm-3 m-md-5">
										<h1 class="h3 mb-3 text-center text-uppercase" ><center><b> curriculum vitae for {{ $user->studentprofile->first_name }} {{ $user->studentprofile->last_name }}</b></center></h1>
									<div class="mb-0">
										<div class="mt-4">
												<h4 class="text-uppercase"><strong>pesonal profile</strong></h4>
												<div class="row">

													<div class="col-lg-6">
														<table class="table table-sm unbordered" width="100%">
															<tbody>
																<tr>
																	<td style="border: none">First Name</td>
																	<td style="border: none">{{ $user->studentprofile->first_name }}</td>
																</tr>
																<tr>
																	<td style="border: none">Middle Name</td>
																	<td style="border: none">{{ $user->studentprofile->middle_name }}</td>
																	</tr>
																	<tr>
																		<td style="border: none">Last Name</td>
																		<td style="border: none">{{ $user->studentprofile->last_name }}</td>
																	</tr>
																	<tr>
																		<td style="border: none">Date of birth</td>
																		<td style="border: none">{{ $user->studentprofile->date_of_birth }}</td>
                                                                    </tr>
                                                                    <tr>
																		<td style="border: none">Gender</td>
																		<td style="border: none">{{ $user->studentprofile->gender }}</td>
                                                                    </tr>
                                                                    <tr>
																		<td style="border: none">Marital status</td>
																		<td style="border: none">{{ $user->studentprofile->marital_status }}</td>
                                                                    </tr>
                                                                    <tr>
																		<td style="border: none">Nationality</td>
																		<td style="border: none">{{ $user->studentprofile->nationality }}</td>
                                                                    </tr>
                                                                    <tr>
																		<td style="border: none">Mobile Phone</td>
																		<td style="border: none">{{ $user->studentprofile->mobile_phone }}</td>
                                                                    </tr>
                                                                    <tr>
																		<td style="border: none">Place of birth</td>
																		<td style="border: none">{{ $user->studentprofile->place_of_birth }}</td>
                                                                    </tr>
                                                                    <tr>
																		<td style="border: none">Disability</td>
																		<td style="border: none">{{ $user->studentprofile->disability }}</td>
																	</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="mt-4">
													<h4 class="text-uppercase"><strong>personal Contancts</strong></h4>
													<div class="row">

														<div class="col-lg-6">
															<table class="table table-sm" width="100%">
																<tbody>
																	<tr >
																		<td style="border: none" >Present Address </td>
																		<td style="border: none">{{ $user->contacts->present_address }}</td>
																	</tr>
																	<tr >
																		<td style="border: none">Permanent Address</td>
																		<td style="border: none">{{ $user->contacts->permenent_address }}</td>
																		</tr>
																		<tr >
																			<td style="border: none">Country Of Residence</td>
																			<td style="border: none">{{ $user->contacts->country }}</td>
																		</tr>
																		<tr >
																			<td style="border: none">City</td>
																			<td style="border: none">{{ $user->contacts->city }}</td>
                                                                        </tr>
                                                                        <tr >
																			<td style="border: none">Province</td>
																			<td style="border: none">{{ $user->contacts->province }}</td>
                                                                        </tr>
                                                                        <tr >
																			<td style="border: none">Mobile</td>
																			<td style="border: none">{{ $user->contacts->mobile }}</td>
                                                                        </tr>
                                                                        <tr >
																			<td style="border: none">Email</td>
																			<td style="border: none">{{ $user->email}}</td>
																		</tr>
																</tbody>
															</table>
														</div>
													</div>
												</div>
									<h4 class="text-uppercase"><strong>educational profile</strong></h4>
									<table class="table table-sm" width="100%">
										<thead>
											<tr style="background-color: #47bac1;">
												<th style="border: none;" align="center">Name of institution</th>
												<th style="border: none;" align="center">Program</th>
												<th style="border: none;" align="center">Level</th>
												<th class="text-center text-white" align="center">Year</th>
											</tr>
										</thead>
										<tbody>
                                            @foreach ($user->academics as $academic )
                                            <tr>
                                                <td style="border: none">{{$academic->institution}}</td>
                                                <td style="border: none">{{$academic->programe}}</td>
                                                <td style="border: none">{{$academic->level}}</td>
                                                <td class="text-right" style="border: none">{{$academic->year}}</td>
                                            </tr>
                                            @endforeach
										</tbody>
									</table>
										<div class="mt-4 pt-2">
												<h4 class="text-uppercase"><strong>personal experience</strong></h4>
												<table class="table table-sm" width="100%">
													<thead>
														<tr style="background-color: #47bac1;">
															<th style="border: none;" align="center">Name of Organisation</th>
															<th style=" border: none;" align="center">Postion</th>
															<th class="text-right text-white" align="center">Duration</th>
														</tr>
													</thead>
													<tbody>
                                                        @foreach ($user->experiences as $experience )
                                                        <tr >
                                                            <td style="border: none">{{$experience->place}}</td>
                                                            <td style="border: none">{{$experience->job_title}}</td>
                                                            <td class="text-right" style="border: none">{{$experience->job_duration}}</td>
                                                        </tr>
                                                        @endforeach
													</tbody>
												</table>
										</div>

										<div class="mt-4 pt-2">
												<h4 class="text-uppercase"><strong>Language</strong></h4>
												<table class="table table-sm" width="100%">
													<thead>
														<tr style="background-color: #47bac1;">
															<th style=" border: none;" align="center">Language</th>
															<th style=" border: none;" align="center">Speaking</th>
															<th style=" border: none;" align="center">Writing</th>
															<th class="text-right text-white" align="center">Reading</th>
														</tr>
													</thead>
													<tbody>
                                                        @foreach ($user->languages  as $language)
                                                        <tr >
                                                                <td style="border: none">{{$language->language}}</td>
                                                                <td style="border: none">{{$language->speaking}}</td>
                                                                <td style="border: none">{{$language->writting}}</td>
                                                                <td class="text-right" style="border: none">{{$language->reading}}</td>
                                                            </tr>
                                                        @endforeach
													</tbody>
												</table>
										</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</main>
		</div>
	</div>
</body>
</html>
