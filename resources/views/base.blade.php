<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'JPGS') }}</title>
        <!-- Fonts -->
        <link href="{{asset('fonts/cerebrisans/cerebrisans-regular')}}" rel="stylesheet">
        <link href="{{asset('fonts/feather/feather.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/template_app.css')}}">
        {{-- <link rel="stylesheet" href="{{asset('css/app.css')}}"> --}}
        <link rel="stylesheet" href="{{asset('css/base.css')}}">
        <style>
            @font-face {
                font-family: 'cerebrisans-regular'; /*a name to be used later*/
                src: url('{{asset("fonts/cerebrisans/cerebrisans-regular.ttf")}}'); /*URL to font*/
            }
        </style>
    </head>
    <body>
        <div class="wrapper" id="root">@yield('content')</div>
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/popper.min.js')}}"></script>
        {{-- <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script> --}}
        <script src="{{asset('js/app.js')}}"></script>

        <script src="{{asset('js/template_app.js')}}"></script>
        {{-- <script src="{{asset('js/chart.js')}}"></script> --}}
        <script src="{{asset('js/forms.js')}}"></script>
        <script src="{{asset('js/maps.js')}}"></script>
        {{-- <script src="{{asset('js/table.js')}}"></script> --}}
        <script src="{{asset('js/jquery.sticky.js')}}"></script>
        <script src="{{asset('js/base.js')}}"></script>
    </body>
</html>