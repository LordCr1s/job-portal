@extends('base')

@section('sidebar')
<nav class="sidebar">
    <div class="sidebar-content">
        <a class="sidebar-brand" href="">
            <i class="align-middle" data-feather="box"></i>
            <span class="align-middle">JPGS</span>
        </a>
        <ul class="sidebar-nav">
            <li class="sidebar-header">
                Main
            </li>
            <li class="sidebar-item active">
                <a href="" class="sidebar-link">
                    <i class="align-middle" data-feather="home"></i> <span class="align-middle">Home</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="" class="sidebar-link">
                    <i class="align-middle" data-feather="user"></i> <span class="align-middle">My Profile</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="" class="sidebar-link">
                    <i class="align-middle" data-feather="disc"></i> <span class="align-middle">Vacancies</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="" class="sidebar-link">
                    <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">My Applications</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="" class="sidebar-link">
                    <i class="align-middle" data-feather="book"></i> <span class="align-middle">Universities</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="" class="sidebar-link">
                    <i class="align-middle" data-feather="briefcase"></i> <span class="align-middle">Companies</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="" class="sidebar-link">
                    <i class="align-middle" data-feather="user-check"></i> <span class="align-middle">Students</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
@endsection