import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Login from './Login';
import{ notify } from './../Content/Utilities'

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            register: true,
            forgetpassword: false,
            login: false 
        };
    }

    handleSubmit = (e)=> {
        e.preventDefault();
        let that = this
        let requestBody = {
            email: $('input[name=email]').val(), 
            password: $('input[name=password]').val(),
            password_confirmation: $('input[name=password_confirmation]').val(),
            role_id: $('#role').val(), 
        }

        fetch('/api/auth/register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody) 
        })
        .then((resp) => resp.json())
        .then(data => {
            if(data.status === 200) {
                this.setState({
                    login: true,
                    register: false,
                }, () => notify('success','Registration complete successiful login to continue','success'))
            }else{
                notify('Failed','There was an error while perfoming','error')
            }
        })
        .catch(error => console.log(error))
    }
    forget = (e)=>{
        e.preventDefault();
        this.setState({
            forgetpassword: true,
            login:false
        })
    }

    register = (e)=>{
        e.preventDefault();
        this.setState({
            register: false,
            login:true
        })
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to='/login'/>;
          } 
          if(this.state.register){
        return (
            <React.Fragment>
                <div className="container h-100">
                    <div className="row h-100">
                        <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                            <div className="d-table-cell align-middle">

                                <div className="text-center mt-4">
                                    <h1 className="h2">JPGS</h1>
                                    <p className="lead">
                                        Register to our system to Post and get latest jobs for graduate Student
                                    </p>
                                </div>

                                <div className="card">
                                    <div className="card-body">
                                        <div className="m-sm-4">
                                            <form onSubmit={this.handleSubmit}>  
                                                <div className="form-group">
                                                    <label>Register As</label>
                                                    <select name="role" id="role" className="form-control form-control-lg" >
                                                        <option defaultValue selected disabled>Select Your Role</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Student</option>
                                                    </select>
                                                </div>                                           
                                                <div className="form-group">
                                                    <label>Email</label>
                                                    <input className="form-control form-control-lg" type="email" name="email" placeholder="Enter your email" />
                                                </div>
                                                <div className="form-group">
                                                    <label>Password</label>
                                                    <input className="form-control form-control-lg" type="password" name="password" placeholder="Enter password" />
                                                </div>
                                                <div className="form-group">
                                                    <label>Confrim Password</label>
                                                    <input className="form-control form-control-lg" type="password" name="password_confirmation" placeholder="Confirm your Password" />
                                                </div>
                                                <div className="text-center mt-3">
                                                    <button type="submit" className="btn btn-lg btn-primary">Register</button>
                                                </div>
                                            </form>
                                            <div className="text-center mt-3">
                                                <Link to="/login" onClick={this.register}>Go Back Login</Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
            }else if(this.state.login){
                return(<Login/>)
            }else if(this.state.forgetpassword){
                return(<ForgetPassword/>) 
            }
    }
}