import React, { Component } from 'react'
import { Redirect } from 'react-router'

export default class Verify extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    handleSubmit(e) {
        e.preventDefault();
        let requestBody = {
            email: $('input[name=email]').val(), 
            password: $('input[name=password]').val()
        }

        fetch('/api/auth/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody) 
        })
        .then((resp) => resp.json())
        .then((data) => {
            localStorage.setItem('access_token', data.access_token);
            localStorage.setItem('user',JSON.stringify(data.user));
            <Redirect to="/materials" push />
           // console.log(data.user)
        
        })
        .catch(error => console.log(error))
    }

    render() {

        return (
            <React.Fragment>
               <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Verify Your Email Address</h5>
                        </div>
                        <div class="card-body">
                            <p class="card-text">A fresh verification link has been sent to your email address.</p>
                            <br/>
                            <p class="card-text">If you did not receive the email</p>
                            <a href="#" class="btn btn-primary">click here to request another</a>
                        </div>
                    </div>
                </div>
            </React.Fragment>

        )
    }
}