import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { authHeader } from './helpers/authHeader'

export default class Sidenav extends Component {

  state = {
    role: {},
    isFetching: true,
  }

  componentDidMount() {
    this.getUser()

  }

  getUser = async () => {
    await fetch('api/auth/me', {
      method: 'POST',
      headers: authHeader()
    })
      .then(res => res.json())
      .then(res => {
        if (res.status == 200 && res.role.name === 'student') {
          this.setState({
            role: res.role,
            isFetching: false
          })
        } else if (res.status == 200 && res.role.name === 'company') {
          this.setState({
            role: res.role,
            isFetching: false
          })
        }
      })
  }

  render() {
    return (
      (!!localStorage.getItem('access_token') && !!localStorage.getItem('user')) ?
        <React.Fragment>
          <nav className="sidebar">
            <div className="sidebar-content" id="sidebar">
              <a className="sidebar-brand" href="">
                <i className="align-middle" data-feather="box"></i>
                <span className="align-middle">JPGS</span>
              </a>
              {this.state.role.name === 'company' ?
                <React.Fragment>
                  <ul className="sidebar-nav">
                    <li className="sidebar-header">
                      Navigations
                        </li>
                    <li className="sidebar-item active">
                      <Link to='/home' className="sidebar-link">
                        <i className="align-middle fe fe-grid mr-2"></i> <span className="align-middle">Dashboard</span>
                      </Link>
                    </li>
                    <li className="sidebar-item">
                      <Link to='/profile' className="sidebar-link">
                        <i className="align-middle fe fe-settings mr-2"></i> <span className="align-middle">Settings</span>
                      </Link>
                    </li>
                    <li className="sidebar-item">
                      <Link to='/applications' className="sidebar-link">
                        <i className="align-middle fe fe-mail mr-2"></i> <span className="align-middle">Applications</span>
                      </Link>
                    </li>
                    <li className="sidebar-item">
                      <Link to='/students' className="sidebar-link">
                        <i className="align-middle fe fe-award mr-2"></i> <span className="align-middle">Graduates</span>
                      </Link>
                    </li>
                  </ul>

                </React.Fragment> :
                <React.Fragment>
                  <ul className="sidebar-nav">
                    <li className="sidebar-header">
                      Navigations
                        </li>
                    <li className="sidebar-item active">
                      <Link to='/home' className="sidebar-link">
                        <i className="align-middle fe fe-grid mr-2"></i> <span className="align-middle">Dashboard</span>
                      </Link>
                    </li>
                    <li className="sidebar-item">
                      <Link to='/profile' className="sidebar-link">
                        <i className="align-middle fe fe-settings mr-2"></i> <span className="align-middle">Settings</span>
                      </Link>
                    </li>
                    <li className="sidebar-item">
                      <Link to='/vacancies' className="sidebar-link">
                        <i className="align-middle fe fe-radio mr-2"></i> <span className="align-middle">Vacancies</span>
                      </Link>
                    </li>
                    <li className="sidebar-item">
                      <Link to='/applications' className="sidebar-link">
                        <i className="align-middle fe fe-mail mr-2"></i> <span className="align-middle">Applications</span>
                      </Link>
                    </li>
                    <li className="sidebar-item">
                      <Link to='/companies' className="sidebar-link">
                        <i className="align-middle fe fe-map-pin mr-2"></i> <span className="align-middle">Companies</span>
                      </Link>
                    </li>
                    <li className="sidebar-item">
                      <Link to='/students' className="sidebar-link">
                        <i className="align-middle fe fe-award mr-2"></i> <span className="align-middle">Graduates</span>
                      </Link>
                    </li>
                  </ul>
                </React.Fragment>
              }
            </div>
          </nav>
        </React.Fragment> : null
    )
  }
}
