import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {GraduatesProfile} from '../Dashboard/Profile'
import BasicInfo from '../Dashboard/BasicInfo'

export default class Graduate extends Component {
  render() {
    return (
        <div className="card mb-2">
            <div className="card-body p-2">
                <div className="row"> 
                    <div className="col-auto">
                        <div className="text-left">
                            <img className="rounded mt-2 ml-2" src={this.props.image} alt="Placeholder" width="110" height="110"/>
                        </div>
                    </div>
                    <div className="col-auto p-1">
                        <h5 className="card-title mb-1 mt-1 text-primary">{this.props.graduate.studentprofile.first_name} </h5>
                        <h6 className="text-dark mb-1"> 
                            <span className="text-muted ml-1">{this.props.graduate.academics.map(data => (data.institution))}</span> 
                        </h6>
                        <h6 className="text-muted mb-1"> 
                            <span className="text-muted ml-1">{this.props.graduate.academics.map(data => (data.programe))}</span> 
                        </h6>
                        <h6 className="text-muted mb-1 ml-1"> 
                            <span className="fe fe-award text-success">
                                <span className="ml-2" style={{fontFamily: 'cerebrisans-regular'}}>{this.props.graduate.academics.map(data => (data.year))}</span>
                            </span>
                        </h6>
                        <div className="pt-1">
                            <a href="" data-toggle="modal" data-target={"#student" + this.props.graduate.studentprofile.first_name}>
                                <span className="fe fe-eye text-primary ml-1">
                                    <span className="ml-2" style={{fontFamily: 'cerebrisans-regular', textDecoration: 'underline'}}>See more </span>
                                </span>
                            </a>

                            <div className="modal fade" id ={"student" + this.props.graduate.studentprofile.first_name} tabIndex="-1" role="dialog" style={{display: 'none'}} aria-hidden="true">
                                <div className="modal-dialog modal-lg" role="document">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title"> {this.props.graduate.studentprofile.first_name}'s more information</h5>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div className="modal-body m-3">
                                            <BasicInfo userData={this.props.graduate} />
                                            <GraduatesProfile userData={this.props.graduate} />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}
