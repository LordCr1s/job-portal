export function notify(message, title, type) {
    toastr[type](message, title, {
        positionClass: "toast-top-right",
        closeButton: $("#toastr-close").prop("checked"),
        progressBar: $("#toastr-progress-bar").prop("checked"),
        newestOnTop: $("#toastr-newest-on-top").prop("checked"),
        rtl: $("body").attr("dir") === "rtl" || $("html").attr("dir") === "rtl",
        timeOut: $("#toastr-duration").val()
    });
}

