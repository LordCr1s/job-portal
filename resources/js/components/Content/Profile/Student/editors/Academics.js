import React, { Component } from 'react'
import { connect } from 'react-redux'
import { authHeader } from './../../../../helpers/authHeader'
import { notify } from './../../../Utilities'

class Academics extends Component {

    state = {
        academic_records : [],
        levels : [
            { value : 0, name : 'Ordinary Level'},
            { value : 1, name : 'Certificate'},
            { value : 2, name : 'Advanced Level'},
            { value : 3, name : 'Diploma'},
            { value : 4, name : 'Advanced Diploma'},
            { value : 5, name : 'Degree'},
            { value : 6, name : 'Post Graduate Diploma'},
            { value : 7, name : 'Master Degree'},
            { value : 8, name : 'PHd'},
            { value : 9, name : 'Short Course'}, 
        ]
    }

    setNewRecord = () => {
        const academic_record = 
        {
            id : 1 + this.props.academic_records.length,
            level : document.getElementById('level').value,
            programe : document.getElementById('programe').value,
            institution : document.getElementById('institution').value,
            year : document.getElementById('year').value,
            has_attachment: false,
            action:'academics'
        }
        this.newAcademicInfo(academic_record)
    }

    deleteRecord = record => {
        this.props.dispatch(this.props.actions.deleteAcademicRecord(record));
    }

    updateRecord = (record, index) => {
        const updated_record = {
            id : record.id,
            level : document.getElementById('level' + record.id).value,
            programe : document.getElementById('programe' + record.id).value,
            institution : document.getElementById('institution' + record.id).value,
            year : document.getElementById('year' + record.id).value,
            has_attachment: false,
            action:'academics'
        }
        this.updateAcademicInfo(updated_record, index)
        document.getElementById('recordedit' + record.id).style.display = 'none';
    }


    getUser = () => {
        fetch('api/auth/me',{
            method: 'POST',
            headers: authHeader(),
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                //console.table(res.user.academics)
                res.user.academics.map(academic => this.props.dispatch(this.props.actions.createAcademicRecord(academic)))
            }
        })
    }

    newAcademicInfo = (new_academic) => {
        fetch('/api/student/store',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(new_academic) 
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                notify('success', 'Academic Record Added', 'success')
                this.props.dispatch(this.props.actions.createAcademicRecord(new_academic))
            }else{
                notify('Failed','There was an error while saving the Academic record','error')
            }
        })
    }

    updateAcademicInfo = (updated_record, index) => {
        fetch('/api/student/update',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(updated_record) 
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.props.dispatch(this.props.actions.updateAcademicRecord(updated_record, index))
            }
        })
    }



    render() {
    return (
        <React.Fragment>
            <div className="card">
                <div className="card-header">
                    <h5 className="card-title text-dark">My certified proffesions</h5>
                </div>
                <table className="table">
                    <tbody>
                        <tr>
                            <td style={{ width: 25 + '%' }}>Level</td>
                            <td style={{ width: 25 + '%'}}>Programe</td>
                            <td style={{ width: 35 + '%'}}>Institution</td>
                            <td className="d-none d-md-table-cell" style={{ width: 15 + '%'}}>Year</td>
                            <td>Actions</td>
                        </tr>
                        {this.props.academic_records.map(record => (
                            <React.Fragment key={ record.id }>
                            <tr>
                                <td>{ record.level }</td>
                                <td>{ record.programe }</td>
                                <td>{ record.institution }</td>
                                <td className="d-none d-md-table-cell">{ record.year }</td>
                                <td className="table-action">
                                    <a onClick={() => document.getElementById('recordedit' + record.id).style.display = 'contents'}>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather text-info feather-edit-2 align-middle">
                                            <polygon points="16 3 21 8 8 21 3 21 3 16 16 3"></polygon>
                                        </svg>
                                    </a>
                                    <a onClick={() => this.deleteRecord(record)}>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather text-danger feather-trash align-middle">
                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                        </svg>
                                    </a>
                                </td>
                            </tr>
                            <tr id={'recordedit' + record.id} style={{ display : 'none' }} className='record-editor'>
                                <td>
                                    <select className="form-control" id= { 'level' + record.id} defaultValue={record.level}>
                                        { this.state.levels.map(level => (
                                            <option key={ level.value }>{ level.name }</option>
                                        ))}
                                    </select>
                                </td>
                                <td>
                                    <input type="text" className="form-control" name="validation-required" 
                                    placeholder="Certificate obtained" id={"programe" + record.id}
                                    defaultValue = {record.programe}></input>
                                </td>
                                <td>
                                    <input type="text" className="form-control" name="validation-required" 
                                    placeholder="Institution" id={"institution" + record.id}
                                    defaultValue={record.institution}></input>
                                </td>
                                <td>
                                    <input className="form-control" type="text" name="datesingle" id={"year" + record.id}
                                    defaultValue={record.year}>
                                    </input>
                                </td>
                                <td className="table-action">
                                    < button type = "submit"
                                    className = "btn btn-primary"
                                    onClick = {
                                        () => this.updateRecord(record, this.props.academic_records.indexOf(record))
                                    } > Update </button>
                                </td>
                            </tr>
                        </React.Fragment>
                        ))}
                        <tr>
                            <td>
                                <span className="text-dark mt-3">Add academic qualifications</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select className="form-control" id= 'level'>
                                    { this.state.levels.map(level => (
                                        <option key={ level.value }>{ level.name }</option>
                                    ))}
                                </select>
                            </td>
                            <td>
                                <input type="text" className="form-control" name="validation-required" 
                                placeholder="Certificate obtained" id='programe'></input>
                            </td>
                            <td>
                                <input type="text" className="form-control" name="validation-required" 
                                placeholder="Institution" id='institution'></input>
                            </td>
                            <td>
                                <input className="form-control" type="text" name="datesingle" id="year">
                                </input>
                            </td>
                            <td className="table-action">
                                <button type="submit" className="btn btn-primary" onClick={() => this.setNewRecord()}>Add</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </React.Fragment> 
    )
  }

  componentDidMount() {
    this.getUser()
    $('#date').ready(
        function () {
            // Select2
        $('.select2').each(function () {
            $(this)
                .wrap('<div class="position-relative"></div>')
                .select2({
                    placeholder: 'Select value',
                    dropdownParent: $(this).parent()
                });
        })
        // Daterangepicker
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        });
        $('input[name="datetimes"]').daterangepicker({
            timePicker: true,
            opens: 'left',
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                format: 'M/DD hh:mm A'
            }
        });
        $('input[name="datesingle"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });
        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);
        cb(start, end);
        }
    );
    
}
}

const mapStateToProps = state => ({
    academic_records: state.student_provider.academic_records,
    actions: state.student_provider.actions,
})

export default connect(mapStateToProps)(Academics);