import React, { Component } from 'react'
import { connect } from 'react-redux'
import { authHeader } from './../../../../helpers/authHeader'
import { notify } from '../../../Utilities';

class ContactDetails extends Component {
        
    state = {
        personal_details: {},
        contact_information: {},
        languages: [],
        academics: [],
        experiences: [],
        user: {}
    }
    saveContactDetails = (e) => {
        e.preventDefault()
        let contacts = {
            present_address: $('#present-addr').val(),
            permenent_address: $('#permenent-addr').val(),
            country: $('#country').val(),
            city: $('#city').val(),
            province: $('#province').val(),
            telephone: $('#telephone').val(),
            mobile: $('#mobile').val(),
            action:'contact'
        }

        fetch('/api/student/update', {
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(contacts) 
        })
        .then((resp) => resp.json())
        .then((data) => {
            if(data.status === 200){
                this.getUser()
                notify('success', 'Contacts Added', 'success')
            }else{
                notify('Failed', 'error occured while adding contacts', 'error')
            }
            
        })
        .catch(error => console.log(error))

        this.props.dispatch(this.props.actions.addOrUpdateContact(contacts));
    }

    componentDidMount(){
        this.getUser()
    }

    getUser = () => {
        fetch('api/auth/me',{
            method: 'POST',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    personal_details: res.user.studentprofile,
                    contact_information: res.user.contacts,
                    languages: res.user.languages,
                    academics: res.user.academics,
                    experiences: res.user.experiences,
                    user: res.user
                })
            }
           // console.log(this.state)
        })
    }

    render() {
        return (
            <React.Fragment>
                <div className="card">
                    <div className="card-header">
                        <h5 className="card-title text-dark">My contact details</h5>
                    </div>
                    <div className="card-body">
                        <form onSubmit={this.saveContactDetails}>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Present address</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" name="validation-required" 
                                    defaultValue={this.state.contact_information.present_address} id="present-addr" placeholder="Present address"></input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Permenent address</label>
                                <div className="col-sm-6">
                                    <input type="text" id="permenent-addr" className="form-control" 
                                    defaultValue={this.state.contact_information.permenent_address} name="validation-required" placeholder="Permenent address">
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Country of residence</label>
                                <div className="col-sm-6">
                                    <input type="text" id="country" className="form-control" 
                                    defaultValue={this.state.contact_information.country} name="validation-required" placeholder="Country of residence">
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">City/State</label>
                                <div className="col-sm-6">
                                    <input type="text" id="city" className="form-control" 
                                    defaultValue={this.state.contact_information.city} name="validation-required" placeholder="City/State">
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Province</label>
                                <div className="col-sm-6">
                                    <input type="text" id="province" className="form-control" 
                                    defaultValue={this.state.contact_information.province} name="validation-required" placeholder="Province">
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Telephone</label>
                                <div className="col-sm-6">
                                    <input type="text" id="telephone" className="form-control" 
                                    defaultValue={this.state.contact_information.telephone} name="validation-required" placeholder="Telephone">
                                    </input>
                                </div>
                            </div>
                            
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Mobile Phone</label>
                                <div className="col-sm-6">
                                    <input type="text" id="mobile" className="form-control" 
                                    defaultValue={this.state.contact_information.mobile} name="validation-required" placeholder="Mobile Phone">
                                    </input>
                                </div>
                            </div>
                            {/* <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Email</label>
                                <div className="col-sm-6">
                                    <input type="email" id="email" className="form-control" 
                                    defaultValue={this.props.contact_information.email} name="validation-required" placeholder="Email">
                                    </input>
                                </div>
                            </div> */}
                            <div className="form-group row">
                                <div className="col-sm-10 ml-sm-auto">
                                    <button type="submit" className="btn btn-primary"
                                    onClick={(e) => this.saveContactDetails(e)}>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    contact_information: state.student_provider.contact_information,
    actions: state.student_provider.actions,
})

export default connect(mapStateToProps)(ContactDetails)