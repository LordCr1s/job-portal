import React, { Component } from 'react'
import { connect } from 'react-redux'
import { authHeader } from './../../../../helpers/authHeader'
import { notify } from './../../../Utilities'

class Language extends Component {

    state = {
        languages : []
    }

    addNewLanguage = () => {
        const new_language = {
            id: 1 + this.props.languages.length,
            language: document.getElementById('language').value,
            speaking: document.getElementById('speaking').value,
            writting: document.getElementById('writting').value,
            reading: document.getElementById('reading').value,
            action:'language'
        }
        this.newLanguageInfo(new_language)
    }

    deleteLanguage = language => {
        this.props.dispatch(this.props.actions.deleteLanguage(language));
    }

    updateLanguage = (language, index) => {
        const updated_language = {
            id: language.id,
            language: document.getElementById('language' + language.id).value,
            speaking: document.getElementById('speaking' + language.id).value,
            writting: document.getElementById('writting' + language.id).value,
            reading: document.getElementById('reading' + language.id).value,
            action:'language'
        }
        this.updateLanguageInfo(updated_language, index)
        document.getElementById('languageedit' + language.id).style.display = 'none';
    }
    componentDidMount(){
        this.getUser()
    }

    getUser = () => {
        fetch('api/auth/me',{
            method: 'POST',
            headers: authHeader(),
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                res.user.languages.map(language => this.props.dispatch(this.props.actions.createLanguage(language)))
            }
        })
    }

    updateLanguageInfo = (updated_language, index) => {
        fetch('/api/student/update',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(updated_language) 
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                notify('success', 'Language Updated', 'success')
                this.props.dispatch(this.props.actions.updateLanguage(updated_language, index))
            }else{
                notify('Failed', 'error occured while updating language', 'error')
            }
        })
    }

    newLanguageInfo = (new_language) => {
        fetch('/api/student/store',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(new_language) 
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                notify('success', 'Language Added', 'success')
                this.props.dispatch(this.props.actions.createLanguage(new_language))
            }else{
                notify('Failed', 'error occured while adding language', 'error')
            }
        })
    }

    render() {
    return (
        <React.Fragment>
            <div className="card">
                <div className="card-header">
                    <h5 className="card-title text-dark">My Language skills</h5>
                </div>
                <table className="table">
                    <tbody>
                        <tr>
                            <td style={{ width: 34 + '%' }}><span className="text-dark">Language</span></td>
                            <td style={{ width: 22 + '%'}}><span className="text-dark">Speaking</span></td>
                            <td style={{ width: 22 + '%'}}><span className="text-dark">Writting</span></td>
                            <td className="d-none d-md-table-cell" style={{ width: 22 + '%'}}><span className="text-dark">Reading</span></td>
                            <td><span className="text-dark">Actions</span></td>
                        </tr>
                        {this.props.languages.map(language => (
                            <React.Fragment key={ language.id }>
                                <tr>
                                    <td>{ language.language }</td>
                                    <td>{ language.speaking }</td>
                                    <td>{ language.writting }</td>
                                    <td className="d-none d-md-table-cell">{ language.reading }</td>
                                    <td className="table-action">
                                        <a onClick={() => document.getElementById('languageedit' + language.id).style.display = 'contents'}>
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather text-info feather-edit-2 align-middle">
                                                <polygon points="16 3 21 8 8 21 3 21 3 16 16 3"></polygon>
                                            </svg>
                                        </a>
                                        <a onClick={() => this.deleteLanguage(language)}>
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather text-danger feather-trash align-middle">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                            </svg>
                                        </a>
                                    </td>
                                </tr>
                                {console.log()}
                                <tr id={'languageedit' + language.id} style={{ display : 'none' }} className='language-editor'>
                                    <td>
                                        <input type="text" className="form-control" name="validation-required" placeholder="language" 
                                        id= { 'language' + language.id} defaultValue={ language.language }></input>
                                    </td>
                                    <td>
                                        <select className="form-control" id={"speaking" + language.id} defaultValue={ language.speaking }>
                                            <option className="col-sm-2">Fluent</option>
                                            <option>Very Good</option>
                                            <option>Good</option>
                                            <option>Confident</option>
                                            <option>Poor</option>													
                                        </select>
                                    </td>
                                    <td>
                                        <select className="form-control" id={"writting" + language.id} defaultValue={ language.writting }>
                                            <option className="col-sm-2">Fluent</option>
                                            <option>Very Good</option>
                                            <option>Good</option>
                                            <option>Confident</option>
                                            <option>Poor</option>													
                                        </select>
                                    </td>
                                    <td>
                                        <select className="form-control" id={"reading" + language.id} defaultValue={ language.reading }>
                                            <option className="col-sm-2">Fluent</option>
                                            <option>Very Good</option>
                                            <option>Good</option>
                                            <option>Confident</option>
                                            <option>Poor</option>													
                                        </select>
                                    </td>
                                    <td className="table-action">
                                        <button type="submit" className="btn btn-info" 
                                            onClick = {
                                                () => this.updateLanguage(language, this.props.languages.indexOf(language))
                                            } >
                                            Update
                                        </button>
                                    </td>
                                </tr>
                            </React.Fragment>
                        ))}
                        <tr>
                            <td>
                                <span className="text-dark mt-3">Add new language</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" className="form-control" name="validation-required" 
                                placeholder="language" id='language'></input>
                            </td>
                            <td>
                                <select className="form-control" id="speaking">
                                    <option defaultValue="Fluent" className="col-sm-2">Fluent</option>
                                    <option>Very Good</option>
                                    <option>Good</option>
                                    <option>Confident</option>
                                    <option>Poor</option>													
                                </select>
                            </td>
                            <td>
                                <select className="form-control" id="writting">
                                    <option defaultValue="Fluent" className="col-sm-2">Fluent</option>
                                    <option>Very Good</option>
                                    <option>Good</option>
                                    <option>Confident</option>
                                    <option>Poor</option>													
                                </select>
                            </td>
                            <td>
                                <select className="form-control" id="reading">
                                    <option defaultValue="Fluent" className="col-sm-2">Fluent</option>
                                    <option>Very Good</option>
                                    <option>Good</option>
                                    <option>Confident</option>
                                    <option>Poor</option>													
                                </select>
                            </td>
                            <td className="table-action">
                                <button type="submit" className="btn btn-primary" onClick={() => this.addNewLanguage()}>Add</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </React.Fragment> 
    )
  }
}

const mapStateToProps = state => ({
    languages: state.student_provider.languages,
    actions: state.student_provider.actions,
})

export default connect(mapStateToProps)(Language)