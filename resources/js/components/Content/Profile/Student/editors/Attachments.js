import React, { Component } from 'react'
import { connect } from 'react-redux';

class Attachments extends Component {

    state = {
        others : []
    }

    actionControl = (record, has_attachment, action) => {
        let actions = {
            add: (record, has_attachment) => {
                // add new attachment 
                this.actionControl(record, true, 'edit')
            },
            edit: (record, has_attachment) => {
                let records = [...this.props.academic_records]
                const index = records.indexOf(record)
                records[index].has_attachment = has_attachment
                console.log(records[index])
                this.props.dispatch(this.props.actions.updateAcademicRecord(records[index], index))
            }
        }
        actions[action](record, has_attachment)
    }

    addAttachment = () => {
        let _title = document.getElementById("attachment-title").value;
        let _file = document.getElementById("attachment-file").value;

        this.props.dispatch(this.props.actions.createOtherAttachment({
            id : this.props.other_attachments.length + 1,
            title : _title,
            file : _file
        }))
    }

    deleteAttachment = (attachment) => {
        this.props.dispatch(this.props.actions.deleteOtherAttachment(attachment))
    }

    editAttachment = (attachment, show_attachment, index) => {
        let editor = document.getElementById("attachment" + attachment.id);
        if (show_attachment) {
            editor.style.display = "contents";
        } else {
            this.props.dispatch(this.props.actions.updateOtherAttachment({
                id: attachment.id,
                title : document.getElementById("attachment-title"+ attachment.id).value,
                file : document.getElementById("attachment-file"+ attachment.id).value
            }, index))
            editor.style.display = "none";
        }
    }

    render() {
    return (
        <React.Fragment>
            <div className="card">
                <div className="card-header">
                    <h5 className="card-title text-dark">Attach certificates of all your accademic qualifications here</h5>
                </div>
                <table className="table">
                    <tbody>
                        <tr>
                            <td style={{ width: 25 + '%' }}>Level</td>
                            <td style={{ width: 25 + '%'}}>Programe</td>
                            <td style={{ width: 30 + '%'}}>Institution</td>
                            <td className="d-none d-md-table-cell" style={{ width: 20 + '%'}}>Year</td>
                            <td>Attach</td>
                        </tr>
                        {this.props.academic_records.map(record => (
                            <React.Fragment key={ record.id }>
                                <tr>
                                    <td>{ record.level }</td>
                                    <td>{ record.programe }</td>
                                    <td>{ record.institution }</td>
                                    <td>{ record.year }</td>
                                    <td>
                                        {!record.has_attachment ? 
                                            <React.Fragment>
                                                <input type='file' id={'file'+record.id}></input>
                                                <button type="submit" className="btn btn-primary mt-1" 
                                                    onClick={() => this.actionControl(record, true, 'add')}>
                                                    Save
                                                </button> 
                                            </React.Fragment>
                                            :
                                            <button type="submit" className="btn btn-warning mt-1" 
                                                onClick={() => this.actionControl(record, false, 'edit')}>
                                                edit
                                            </button>
                                        }
                                    </td>
                                </tr>
                            </React.Fragment>
                        ))}
                    </tbody>
                </table>
                <div>
                    <span className="text-dark ml-3 mt-3">Add other attachments</span>
                </div>
                <table className="table">
                    <tbody>
                        <tr>
                            <td style={{ width: 40 + '%' }}>Title</td>
                            <td style={{ width: 35 + '%'}}>Attachment</td>
                            <td style={{ width: 25 + '%'}}>Action</td>
                        </tr>
                        {this.props.other_attachments.map(attachment => (
                            <React.Fragment key={ attachment.id }>
                                <tr>
                                    <td>{ attachment.title }</td>
                                    <td>{ attachment.file }</td>
                                    <td className="table-action">
                                        <a onClick={() => this.editAttachment(attachment, true, this.props.other_attachments.indexOf(attachment))}>
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-edit-2 align-middle">
                                                <polygon points="16 3 21 8 8 21 3 21 3 16 16 3"></polygon>
                                            </svg>
                                        </a>
                                        <a onClick={() => this.deleteAttachment(attachment)}>
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-trash align-middle">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                            </svg>
                                        </a>
                                    </td>
                                </tr>
                                <tr style={{ display: "none"}} id={"attachment" + attachment.id}>
                                    <td>
                                        <input type="text" className="form-control" name="validation-required" 
                                        defaultValue={attachment.title} id={"attachment-title" + attachment.id}></input>
                                    </td>
                                    <td>
                                        <input type='file' id={"attachment-file" + attachment.id}></input>
                                    </td>
                                    <td className="table-action">
                                        <button type="submit" className="btn btn-primary mt-1" 
                                            onClick={() => this.editAttachment(attachment, false, this.props.other_attachments.indexOf(attachment))}>
                                            Update
                                        </button> 
                                    </td>
                                </tr>
                            </React.Fragment>
                        ))}
                        <tr>
                            <td>
                                <input type="text" className="form-control" name="validation-required" 
                                placeholder="Certificate obtained" id={"attachment-title"}></input>
                            </td>
                            <td>
                                <input type='file' id={"attachment-file"}></input>
                            </td>
                            <td className="table-action">
                                <button type="submit" className="btn btn-primary mt-1" 
                                    onClick={() => this.addAttachment()}>
                                    Add
                                </button> 
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </React.Fragment> 
    )
  }
}


const mapStateToProps = (state) => ({
    academic_records: state.student_provider.academic_records,
    other_attachments: state.student_provider.other_attachments,
    actions: state.student_provider.actions,
})

export default connect(mapStateToProps)(Attachments)