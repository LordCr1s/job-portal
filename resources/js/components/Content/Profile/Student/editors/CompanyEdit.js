import React, { Component } from 'react';
import AvatarEditor from 'react-avatar-editor';
import { connect } from 'react-redux'
import { authHeader } from './../../../../helpers/authHeader'
import { notify } from '../../../Utilities'

class CompanyEdit extends Component {

    state = {
        company_details: {},
    }

    updateCompanyAbout = () => {

    }


    saveCompanyDetails = event => {
        event.preventDefault()
       
        const company_details = {
            company_name: $('#company_name').val(),
            website: $('#website').val(),
            number_of_employees: $('#number_of_employee').val(),
            industry: $('#industry').val(),
            contact_person: $('#contact_person').val(),
            email_for_notification: $('#email_for_notification').val(),
            phone_number: $('#phone_number').val(),
            address: $('#address').val(),
            country: $('#country').val(),
            about: $('#about').val(),
            action: 'company_info'
        }

        fetch('/api/company/store', {
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(company_details) 
        })
        .then((resp) => resp.json())
        .then((data) => {
            if(data.status === 200){ 
                this.getUser()
                notify('success', 'Company Details', 'success')
            }else{
                notify('Failed', 'the was an while trying to update company details', 'error') 
               }
        })
        .catch(error => console.log(error))
    }

    render() {
        return (
            <React.Fragment>
                <div className="card">
                    <div className="card-header">
                        <h5 className="card-title text-dark">Provide your Company details</h5>
                    </div>
                    <div className="card-body">
                        <form encType="multipart/form-data" onSubmit={this.saveCompanyDetails}>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Company name</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="company_name" 
                                    name="validation-required" placeholder="Company's Name" defaultValue={this.state.company_details.company_name}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Website</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="website" 
                                    name="validation-required" placeholder="Company's website" defaultValue={this.state.company_details.website}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Number of Employee</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="number_of_employee" 
                                    name="validation-required" placeholder="Company's Number of Employee" defaultValue={this.state.company_details.number_of_employees}>
                                    </input>
                                </div>
                            </div>
                        
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Industry</label>
                                <div className="col-sm-6">
                                    <input className="form-control" id="industry" type="text" placeholder="Industry"
                                    defaultValue={this.state.company_details.industry}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Contact Person</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="contact_person" 
                                    name="validation-required" placeholder="Contact Person" defaultValue={this.state.company_details.contact_person}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Email For Notification</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="email_for_notification" 
                                    name="validation-required" placeholder="Email For Notification" defaultValue={this.state.company_details.email_for_notification}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Phone Number</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="phone_number" defaultValue={this.state.company_details.phone_number}
                                    name="validation-required" placeholder="Phone Number">
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Address</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="address" 
                                    name="validation-required" placeholder="Address" defaultValue={this.state.company_details.address}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Country</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="country" 
                                    name="validation-required" placeholder="country" defaultValue={this.state.company_details.country}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">About</label>
                                <div className="col">
                                    <textarea className="form-control" id="about"></textarea>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-10 ml-sm-auto">
                                    <button type="submit" className="btn btn-primary" onClick={event => this.saveCompanyDetails(event)}>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    componentDidMount() {
        this.getUser()
        $('#date').ready(
            function () {
                // Select2
            $('.select2').each(function () {
                $(this)
                    .wrap('<div class="position-relative"></div>')
                    .select2({
                        placeholder: 'Select value',
                        dropdownParent: $(this).parent()
                    });
            })
            // Daterangepicker
            $('input[name="daterange"]').daterangepicker({
                opens: 'left'
            });
            $('input[name="datetimes"]').daterangepicker({
                timePicker: true,
                opens: 'left',
                startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),
                locale: {
                    format: 'M/DD hh:mm A'
                }
            });
            $('input[name="datesingle"]').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);
            cb(start, end);
            }
        );

        
    }

    getUser = () => {
        fetch('api/auth/me',{
            method: 'POST',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    company_details: res.user.companyprofile,
                })
            }
           // console.log(this.state)
        })
    }
}

const mapStateToProps = state => ({
    company_details: state.student_provider.company_details,
    actions: state.student_provider.actions,
})

export default connect(mapStateToProps)(CompanyEdit)