import React, { Component } from 'react';
import AvatarEditor from 'react-avatar-editor';
import { connect } from 'react-redux'
import { authHeader } from './../../../../helpers/authHeader'
import { notify } from './../../../Utilities'

class PersonalDetails extends Component {

    state = {
        profile_image_source: require('./img/user.png'),
        personal_details: {},
        contact_details: {},
        languages: [],
        academics: [],
        experiences: [],
        user: {}
    }

    updateProfilePicture = event =>{
        event.preventDefault()
        const formdata = new FormData();
        const image = $('#profile_image').prop('files')[0]
        formdata.append('image', image);
      
        axios.post('api/student/update_image', formdata,{headers: authHeader()})
        .then((data)=> {
           if(data.status === 200){
            notify('success', 'Photo Updated', 'success')
           }else{
            notify('Failed', 'there was an error while uploading the photo', 'error') 
           }
        })
    }

    savePersonalDetails = event => {
        event.preventDefault()
        $('#profile_image').on('change', function () {
            var fileReader = new FileReader();
            fileReader.onload = function () {
              var data = fileReader.result;  // data <-- in this var you have the file data in Base64 format
            };
            console.log(fileReader.readAsDataURL($('#profile_image').prop('files')[0]));
        });
        var fileReader = new FileReader();
        fileReader.onload = function () {
          var data = fileReader.result;  
        };

        const personal_details = {
            first_name: $('#first_name').val(),
            middle_name: $('#middle_name').val(),
            last_name: $('#last_name').val(),
            date_of_birth: $('#date_of_birth').val(),
            gender: $('#gender').val(),
            marital_status: $('#marital_status').val(),
            nationality: $('#nationality').val(),
            mobile_phone: $('#mobile_phone').val(),
            place_of_birth: $('#place_of_birth').val(),
            disability: $('#disability').val(),
            image: fileReader.readAsDataURL($('#profile_image').prop('files')[0]),
            action: 'personal_info'
        }

        fetch('/api/student/update', {
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(personal_details) 
        })
        .then((resp) => resp.json())
        .then((data) => {
            if(data.status === 200){
                this.getUser()
                notify('success', 'Personal Details Updated', 'success')
            }else{
                notify('Failed', 'there was an error while updating Personal Details ', 'error') 
               }
            
        })
        .catch(error => console.log(error))

        this.props.dispatch(this.props.actions.addUpdatePersonalDetail(personal_details))
        if (this.editor) {
            document.getElementById('test').appendChild(this.editor.getImageScaledToCanvas())
        }
    }

    setEditorRef = (editor) => this.editor = editor

    render() {
        return (
            <React.Fragment>
                <div className="card">
                    <div className="card-header">
                        <h5 className="card-title text-dark">Provide your personal details</h5>
                    </div>
                    <div className="card-body">
                        <form encType="multipart/form-data" method="POST" onSubmit={this.updateProfilePicture} >
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Profile image</label>
                                <div className="col-sm-6">
                                    <input type="file" className="form-control" id="profile_image" accept="image/*" style={{border: "none"}} 
                                    name="image" placeholder="first name" defaultValue={this.state.personal_details.image}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-10 ml-sm-auto mb-3">
                                    <button type="submit" className="btn btn-primary">update profile image</button>
                                </div>
                            </div>
                        </form>
                        <form encType="multipart/form-data" onSubmit={this.savePersonalDetails}>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">First name</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="first_name" 
                                    name="validation-required" placeholder="first name" defaultValue={this.state.personal_details.first_name}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Middle name</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="middle_name" 
                                    name="validation-required" placeholder="middle name" defaultValue={this.state.personal_details.middle_name}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Last name</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="last_name" 
                                    name="validation-required" placeholder="last name" defaultValue={this.state.personal_details.last_name}>
                                    </input>
                                </div>
                            </div>
                        
                            <div className="form-group row" id="date">
                                <label className="col-form-label col-sm-2 text-sm-right">Date of Birth</label>
                                <div className="col-sm-6">
                                    <input className="form-control" id="date_of_birth" type="text" name="datesingle"
                                    defaultValue={this.state.personal_details.date_of_birth}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Gender</label>
                                <div className="col-sm-6">
                                    <select className="form-control mb-3" id="gender" defaultValue={this.state.personal_details.gender}>
                                        <option defaultValue="Male" className="col-sm-2">Male</option>
                                        <option>Female</option>													
                                    </select>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Marital Status</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="marital_status" 
                                    name="validation-required" placeholder="Marital Status" defaultValue={this.state.personal_details.marital_status}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Nationality</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="nationality" defaultValue={this.state.personal_details.nationality}
                                    name="validation-required" placeholder="nationality eg: Tanzania">
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Mobile Phone</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="mobile_phone" 
                                    name="validation-required" placeholder="mobile phone" defaultValue={this.state.personal_details.mobile_phone}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Place of birth</label>
                                <div className="col-sm-6">
                                    <input type="text" className="form-control" id="place_of_birth" 
                                    name="validation-required" placeholder="place of birth" defaultValue={this.state.personal_details.place_of_birth}>
                                    </input>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-form-label col-sm-2 text-sm-right">Disability</label>
                                <div className="col-sm-6">
                                    <select className="form-control mb-3" id="disability" defaultValue={this.state.personal_details.disability}>
                                        <option value="No" className="col-sm-2">No</option>
                                        <option value="Yes">Yes</option>													
                                    </select>
                                </div>
                            </div>
                            
                            <div className="form-group row">
                                <div className="col-sm-10 ml-sm-auto">
                                    <button type="submit" className="btn btn-primary" onClick={event => this.savePersonalDetails(event)}>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    componentDidMount() {
        this.getUser()
        $('#date').ready(
            function () {
                // Select2
            $('.select2').each(function () {
                $(this)
                    .wrap('<div class="position-relative"></div>')
                    .select2({
                        placeholder: 'Select value',
                        dropdownParent: $(this).parent()
                    });
            })
            // Daterangepicker
            $('input[name="daterange"]').daterangepicker({
                opens: 'left'
            });
            $('input[name="datetimes"]').daterangepicker({
                timePicker: true,
                opens: 'left',
                startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),
                locale: {
                    format: 'M/DD hh:mm A'
                }
            });
            $('input[name="datesingle"]').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);
            cb(start, end);
            }
        );

        
    }

    getUser = () => {
        fetch('api/auth/me',{
            method: 'POST',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    personal_details: res.user.studentprofile,
                    contact_details: res.user.contacts,
                    languages: res.user.languages,
                    academics: res.user.academics,
                    experiences: res.user.experiences,
                    user: res.user
                })
            }
           // console.log(this.state)
        })
    }
}

const mapStateToProps = state => ({
    personal_details: state.student_provider.personal_details,
    actions: state.student_provider.actions,
})

export default connect(mapStateToProps)(PersonalDetails)