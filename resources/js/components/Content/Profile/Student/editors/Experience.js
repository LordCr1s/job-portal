import React, { Component } from 'react'
import { connect } from "react-redux"
import { authHeader } from './../../../../helpers/authHeader'
import { notify } from './../../../Utilities'

class Experience extends Component {

    state = {
        experiences : []
    }

    addExperience = () => {

        const experience = {
            id : this.props.experiences.length + 1,
            place : document.getElementById("experience-place").value,
            title : document.getElementById("experience-title").value,
            duration : document.getElementById("experience-date").value,
            action: 'experience'
        }
        this.newExperienceInfo(experience)
    }

    deleteExperience = (experience) => {
        this.props.dispatch(this.props.actions.deleteExperience(experience))
    }

    editExperience = (experience, show_experience, index) => {

        let editor = document.getElementById("experience" + experience.id);

        if (show_experience) {
            editor.style.display = "contents";
        } else {
            const update_experience = {
                id: experience.id,
                place: document.getElementById("experience-place"+ experience.id).value,
                title: document.getElementById("experience-title"+ experience.id).value,
                duration: document.getElementById("experience-date"+ experience.id).value,
                action: 'experience'
            }
            this.updateExperienceInfo(update_experience, index)
            editor.style.display = "none";
        }
    }

    getUser = () => {
        fetch('api/auth/me',{
            method: 'POST',
            headers: authHeader(),
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                
                res.user.experiences.map(experience => this.props.dispatch(this.props.actions.createExperience(experience)))
            }
        })
    }

    newExperienceInfo = (experience) => {
        fetch('/api/student/store',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(experience) 
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                notify('success', 'Experiense Added', 'success')
                this.props.dispatch(this.props.actions.createExperience(experience))
            }else{
                notify('Failed', 'error occured while adding experiense', 'error')
            }
        })
    }

    updateExperienceInfo = (experience, index) => {
        fetch('/api/student/update',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(experience) 
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                notify('success', 'Experiense updated', 'success')
                this.props.dispatch(this.props.actions.updateExperience(experience, index))
            }else{
                notify('Failed', 'error occured while updating experiense', 'error') 
            }
        })
    }


    render() {
    return (
        <React.Fragment>
            <div className="card">
                <div className="card-header">
                    <h5 className="card-title text-dark">List your work experience</h5>
                </div>
                <table className="table">
                    <tbody>
                        <tr>
                            <td style={{ width: 30 + '%' }}>Place</td>
                            <td style={{ width: 30 + '%'}}>Job Title</td>
                            <td style={{ width: 25 + '%'}}>Job duration</td>
                            <td style={{ width: 15 + '%'}}>Action</td>
                        </tr>
                        {this.props.experiences.map(experience => (
                            <React.Fragment key={ experience.id }>
                                <tr>
                                    <td>{ experience.place }</td>
                                    <td>{ experience.title }</td>
                                    <td>{ experience.duration }</td>
                                    <td className="table-action">
                                        <a onClick={() => this.editExperience(experience, true, this.props.experiences.indexOf(experience))}>
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather text-info feather-edit-2 align-middle">
                                                <polygon points="16 3 21 8 8 21 3 21 3 16 16 3"></polygon>
                                            </svg>
                                        </a>
                                        <a onClick={() => this.deleteExperience(experience)}>
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather text-danger feather-trash align-middle">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                            </svg>
                                        </a>
                                    </td>
                                </tr>
                                <tr style={{ display: "none"}} id={"experience" + experience.id}>
                                    <td>
                                        <input type="text" className="form-control" name="validation-required" 
                                        defaultValue={experience.place} id={"experience-place" + experience.id}></input>
                                    </td>
                                    <td>
                                        <input type="text" className="form-control" name="validation-required" 
                                        defaultValue={experience.title} id={"experience-title" + experience.id}></input>
                                    </td>
                                    <td>
                                        <input type="text" className="form-control" name="validation-required" 
                                        defaultValue={experience.duration} id={"experience-date" + experience.id}></input>
                                    </td>
                                    <td className="table-action">
                                        <button type="submit" className="btn btn-primary mt-1" 
                                            onClick={() => this.editExperience(experience, false, this.props.experiences.indexOf(experience))}>
                                            Update
                                        </button> 
                                    </td>
                                </tr>
                            </React.Fragment>
                        ))}
                        <tr>
                            <td>
                                <span className="text-dark mt-3">Add work experience</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" className="form-control" name="validation-required" 
                                placeholder="Place you've worked" id={"experience-place"}></input>
                            </td>
                            <td>
                                <input type="text" className="form-control" name="validation-required" 
                                placeholder="You were working as" id={"experience-title"}></input>
                            </td>
                            <td>
                                <input className="form-control" type="text" name="daterange" defaultValue="" id="experience-date" />
                            </td>
                            <td className="table-action">
                                <button type="submit" className="btn btn-primary mt-1" 
                                    onClick={() => this.addExperience()}>
                                    Add
                                </button> 
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </React.Fragment> 
    )
  }

  componentDidMount = () => {

    $('#experience-date').ready( function(event) {
        // Select2
        $('.select2').each(function() {
            $(this)
                .wrap('<div class="position-relative"></div>')
                .select2({
                    placeholder: 'Select value',
                    dropdownParent: $(this).parent()
                });
        })
        // Daterangepicker
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        });

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);
        cb(start, end);
    });
  }
}


const mapStateToProps = state => ({
    experiences: state.student_provider.experiences,
    actions: state.student_provider.actions
})

export default connect(mapStateToProps)(Experience)