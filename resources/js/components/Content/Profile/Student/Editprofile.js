import React, { Component } from 'react';
import PersonalDetails from './editors/PersonalDetails';
import ContactDetails from './editors/ContactDetails';
import Language from './editors/Language';
import Academics from './editors/Academics';
import Attachments from './editors/Attachments';
import Experience from './editors/Experience';
import CompanyEdit from './editors/CompanyEdit'

export default class Editprofile extends Component {

  render() {
    return (
        <React.Fragment>
            <div className="col-12">
                <div className="tab">
                    <Headings showProfile={this.props.showProfile} role={this.props.role}/>
                    <div className="tab-content">
                        <TabContents role={this.props.role}/>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
  }
}

class Headings extends Component {
    render() {
        return (
            <React.Fragment>
                { this.props.role.name === 'student'? 
                <React.Fragment>
                <div className="card-header">
                <ul className="nav nav-pills card-header-pills pull-right" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active" href="#icon-tab-1" data-toggle="tab" role="tab">
                            <i className="align-middle fe fe-user"></i>
                            <span className="align-middle ml-2">Personal details</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#icon-tab-2" data-toggle="tab" role="tab">
                            <i className="align-middle fe fe-slack"></i>
                            <span className="align-middle ml-2">Contacts</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#icon-tab-3" data-toggle="tab" role="tab">
                            <i className="align-middle fe fe-globe"></i>
                            <span className="align-middle ml-2">Language</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#icon-tab-4" data-toggle="tab" role="tab">
                            <i className="align-middle fe fe-award"></i>
                            <span className="align-middle ml-2">Academics</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#icon-tab-5" data-toggle="tab" role="tab">
                            <i className="align-middle fe fe-wind"></i>
                            <span className="align-middle ml-2">Experience</span>
                        </a>
                    </li>
                </ul>
            </div>
            </React.Fragment>: 
            <React.Fragment>
                <div className="card-header">
                <ul className="nav nav-pills card-header-pills pull-right" role="tablist">
                 <li className="nav-item">
                        <a className="nav-link" href="#icon-tab-6" data-toggle="tab" role="tab">
                            <i className="align-middle fe fe-paperclip"></i>
                            <span className="align-middle ml-2">Company Informations</span>
                        </a>
                    </li> 
                </ul>
            </div>
            </React.Fragment>
            }   
            </React.Fragment> 
        )
    }
}

class TabContents extends Component {
    render() {
        return (
            <React.Fragment>
            { this.props.role.name === 'student'? 
                <React.Fragment>
                <div className="tab-pane fade show active" id="icon-tab-1" role="tabpanel">
                    <PersonalDetails />
                </div>
                <div className="tab-pane fade" id="icon-tab-2" role="tabpanel">
                    <ContactDetails />
                </div>
                <div className="tab-pane fade" id="icon-tab-3" role="tabpanel">
                    <Language />
                </div>
                <div className="tab-pane fade" id="icon-tab-4" role="tabpanel">
                    <Academics />
                </div>
                <div className="tab-pane fade" id="icon-tab-5" role="tabpanel">
                    <Experience />
                </div>
                </React.Fragment>:
                <React.Fragment>
                 <div className="tab-pane fade show active" id="icon-tab-6" role="tabpanel">
                    <CompanyEdit />
                </div> 
                </React.Fragment>
            }
            </React.Fragment>
        )
    }
}