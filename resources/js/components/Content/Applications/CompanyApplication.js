import React, { Component } from 'react'
import { authHeader } from './../../helpers/authHeader'
import { notify } from '../Utilities'
import { Redirect } from 'react-router-dom';


export default class CompanyApplication extends Component {
   state ={
       redirect: false,
       path: ''
   }
  // change the color of the badge depending on the application status
  setStatus = status => {
    if (status == 'pending')
        return <span className="mr-2 badge badge-warning">{status}</span>
    else if (status == 'accepted')
        return <span className="mr-2 badge badge-success">{status}</span>
    else 
        return <span className="mr-2 badge badge-danger">{status}</span>
  }

  rejectApplication = (app_id ,sts)=>{
    const application = {
        application_id : app_id,
        status: sts
    }
    fetch('/api/application/reject', {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify(application) 
    })
    .then((resp) => resp.json())
    .then((data) => {
        if(data.status === 200){
            notify('success', 'Application Rejected', 'success')
        }else{
            notify('Failed', 'there was an error while rejecting the appliction', 'error') 
           }
    })
    .catch(error => console.log(error))
  }

  acceptApplication = (app_id ,sts)=>{
    const application = {
        application_id : app_id,
        status: sts
    }
    fetch('/api/application/accept', {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify(application) 
    })
    .then((resp) => resp.json())
    .then((data) => {
        if(data.status === 200){
            notify('success', 'Application Accepted', 'success')
        }else{
            notify('Failed', 'there was an error while accepted the appliction', 'error') 
           }
    })
    .catch(error => console.log(error))
 }

 downloadcvApplication = (app_id, sts)=>{
    const application = {
        application_id : app_id,
        status: sts
    }
    fetch('/api/application/downloadcv/'+ application.application_id, {
        method: 'GET',
        headers: authHeader(),
    })
    .then((resp) => resp.json())
    .then((data) => {
        if(data.status === 200){
            this.setState({
                path: data.file,
                redirect:true,
            })
            notify('success', 'Applicant CV Downloaded', 'success')
        }else{
            notify('Failed', 'there was an error while downloading applicant CV the ', 'error') 
           }
    })
    .catch(error => console.log(error))
}

downloadapplaterApplication = (app_id, sts)=>{
    const application = {
        application_id : app_id,
        status: sts
    }
    fetch('/api/application/downloadappletter/'+ application.application_id, {
        method: 'GET',
        headers: authHeader(),
      
    })
    .then((resp) => resp.json())
    .then((data) => {
        if(data.status === 200){
            this.setState({
                path: data.file,
                redirect:true,
            })
            notify('success', 'Applicant Application later downloaded', 'success')
        }else{
            notify('Failed', 'there was an error while downloading application later for the appliction', 'error') 
           }
    })
    .catch(error => console.log(error))
}

  render() {
    const application1 = this.props.application
    if(this.state.redirect){
        return  <Redirect to={this.state.path}/>
    }else{

    
    return (
        application1.applications.map(application=>(
        <div className="card mb-1">
            <div className="card-body p-2">
                <div className="row">
                    <div className="col-2" style={{lineHeight: 2.5}}>
                        <span className="mr-2">{application.user == null? 'JPGS': application.user.studentprofile.first_name} {application.user == null? 'JPGS':application.user.studentprofile.last_name}</span>
                    </div>
                    <div className="col-2 p-0 m-0" style={{lineHeight: 2.5}}>
                        <span className="mr-2 text-primary">{application1.title}</span> 
                    </div>
                    <div className="col-2 p-0 m-0" style={{lineHeight: 2.5}}>
                        <span className="mr-2 text-muted">
                            <span className="fe fe-clock">
                                <span className="ml-2" style={{fontFamily: 'cerebrisans-regular'}}>
                                    {application.created_at}
                                </span>
                            </span>
                        </span>
                    </div>
                    <div className="col-1 p-0 m-0" style={{lineHeight: 2.5}}>
                        {this.setStatus(application.status)}
                    </div>
                    <div className="col-5 p-0 m-0">
                        <button className="btn btn-success btn-sm ml-2" onClick={() => this.acceptApplication(application.id, 'accepted')}>
                            Accept
                        </button>
                        <button className="btn btn-primary btn-sm ml-2" onClick={() => this.downloadcvApplication(application.id, 'cv')}>
                            Download CV
                        </button>
                        <button className="btn btn-secondary btn-sm ml-2" onClick={() => this.downloadapplaterApplication(application.id, 'later')}>
                            Download App Latter
                        </button>
                        <button className="btn btn-danger btn-sm ml-2" onClick={() => this.rejectApplication(application.id, 'rejected')}>
                            Reject
                        </button>
                    </div>
                </div>
            </div>
        </div>
        ))

    )}
  }
}
