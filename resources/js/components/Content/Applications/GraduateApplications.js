import React, { Component } from 'react'
import { authHeader } from './../../helpers/authHeader'
import { notify } from '../Utilities'

export default class GraduateApplications extends Component {

    // change the color of the badge depending on the application status
    setStatus = status => {
        if (status == 'pending')
            return <span className="mr-2 text-warning">{status}</span>
        else if (status == 'accepted')
            return <span className="mr-2 text-success">{status}</span>
        else 
            return <span className="mr-2 text-danger">{status}</span>
    }
    

    cancelApplication = (app_id ,sts)=>{
        const application = {
            application_id : app_id,
            status: sts
        }
        fetch('/api/application/cancel', {
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(application) 
        })
        .then((resp) => resp.json())
        .then((data) => {
            if(data.status === 200){
                notify('success', 'Application Canceled', 'success')
            }else{
                notify('Failed', 'there was an error while canceling the appliction', 'error') 
               }
        })
        .catch(error => console.log(error))
    }

  render() {
    let application = this.props.application
    
    return (
        application.vacancy == null ? null :
        <div className="card mb-1">
            <div className="card-body" style={{ padding: .1 + 'rem'}}>
                <div className="row">
                    <div className="col-3" style={{lineHeight: 2.5}}>
                        <span className="ml-2 text-muted">{application.vacancy.title}</span>
                    </div>
                    <div className="col-4 p-0 m-0" style={{lineHeight: 2.5}}>
                        <span className="mr-2 text-muted">{application.vacancy.user == null ? 'JPGS':application.vacancy.user.companyprofile.company_name  }</span>
                    </div>
                    <div className="col-2 p-0 m-0" style={{lineHeight: 2.5}}>
                        <span className="mr-2 text-muted">
                            <span className="fe fe-watch text-dark">
                                <span className="ml-1 text-muted" style={{fontFamily: 'cerebrisans-regular'}}>
                                    {application.created_at}
                                </span>
                            </span>
                        </span>
                    </div>
                    <div className="col-2 p-0 m-0" style={{lineHeight: 2.5}}>
                        {this.setStatus(application.status)}
                    </div>
                    <div className="col-1 p-0 m-0">
                        <button className="btn btn-danger" onClick={() => this.props.cancelApplication(application, 'canceled')}>
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}
        