import React, { Component } from 'react'

export default class Vacancy extends Component {
    render() {
        return(
            <React.Fragment>
                <h5 className="card-title">{this.props.vacancy.title}</h5>
                <h6 className="card-subtitle text-small text-muted pb-2">
                    <span className="text-muted">
                        {/* <span className="fe fe-map-pin text-warning ml-1">
                        {this.props.vacancy.user.companyprofile.company_name} 
                        </span> */}
                    </span>
                </h6>
                      
                <span className="fe fe-watch">
                    <span className="ml-2 text-muted" style={{fontFamily: 'cerebrisans-regular'}}>
                        Published on {this.props.vacancy.created_at} 
                    </span>
                </span> <br />
                <span className="fe fe-watch">
                    <span className="ml-2 text-muted" style={{fontFamily: 'cerebrisans-regular'}}>
                        Application Deadline is on {this.props.vacancy.application_deadline}
                    </span>
                </span>
            </React.Fragment>
        )
    }
}
