import React, { Component } from 'react'

export default class MoreDetails extends Component {
  render() {
    return(
        <React.Fragment>
          <div id="changelog">
            <h4>JOB DESCRIPTION</h4>
            <h3>{this.props.vacancy.title}</h3>
            <p>Deadline: {this.props.vacancy.application_deadline}</p>
            <p>Location: {this.props.vacancy.location.name}</p>
            <p>Category: {this.props.vacancy.industry.name}</p>
          </div>
          <div id="changelog">
           {this.props.vacancy.body}
          </div><br></br>
          {/* <div id="changelog">
            <h4>COMPANY INFO</h4>
            <p>Name: {this.props.vacancy.user.companyprofile.company_name}</p>
            <p>Website: {this.props.vacancy.user.companyprofile.website}</p>
            <p>Email: {this.props.vacancy.user.companyprofile.email_for_notification}</p>
            <p>Mobile: {this.props.vacancy.user.companyprofile.phone_number}</p>
            <p>Address: {this.props.vacancy.user.companyprofile.address}</p>
          </div> */}
        </React.Fragment>
    )
  }
}