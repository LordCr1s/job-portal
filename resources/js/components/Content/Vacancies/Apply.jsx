import React, { Component } from 'react'
import { authHeaderUpload, authHeader } from './../../helpers/authHeader'
import axios from 'axios'
import { notify } from '../Utilities'

export default class Apply extends Component {
    
    state = {
        feedback: {
            show_feedback: false,
            is_feedback_success: false
        }
    }

    applyThisJob = event => {
        event.preventDefault()
        const formdata = new FormData();
        const app_letter = $('#application-letter'+ this.props.vacancy.id).prop('files')[0]
        const vacancy_id = $('#vacancy'+ this.props.vacancy.id).val()
        formdata.append('later', app_letter);
        formdata.append('vacancy_id', vacancy_id);
        axios.post('api/application/store', formdata,{headers: authHeader()})
       
        .then( data => {
            if(data.status === 200){
                notify('success', 'Application submited successful', 'success')
            }else{ 
                notify('Failed', 'error occured while submiting your Application', 'error')
            }
            // this.setState({
            //     is_feedback_success: true,
            // })

        })
        // let feedback = {...this.state.feedback}

        // if(app_letter == ''){
        //    feedback.show_feedback = true
           
        //    this.setState({feedback})
        // }
        // else{
        //     this.props.vacancy.id
        //     feedback.show_feedback = true
        //     feedback.is_feedback_success = true
        //     this.setState({feedback})
        // }
            
    }

    render() {
        return(
            <React.Fragment>
                {this.state.feedback.show_feedback ?  
                     <Alert is_feedback_success={this.state.feedback.is_feedback_success}/>: <div></div>
                }
                <div id="changelog">
                    <h4>{this.props.vacancy.title}</h4>
                    <p>I declare that the information provided in my profile is complete and correct to the 
                        best of my knowledge and matches the criteria for this Post ({this.props.vacancy.title}).
                        I understand that any false information supplied could lead to 
                        my application being disqualified or my discharge if I am appointed.</p>
                    <p>Please attach your Appliation letter Here.</p>
                    <form action="POST"  onSubmit={this.applyThisJob} encType="multipart/form-data" id={ 'form' + this.props.vacancy.id}>
                        <div>
                            <input type='file' id={'application-letter' + this.props.vacancy.id}></input>
                            <input type='text' hidden id={'vacancy' + this.props.vacancy.id} defaultValue={this.props.vacancy.id}></input>
                        </div>
                        <div className="pt-3">
                            <button type="submit" className="btn btn-sm btn-primary" >
                                Confirm Application
                            </button> 
                        </div>
                    </form>
                   
                </div>
            </React.Fragment>
        )
    }
}

class Alert extends Component {
    render() {
        return(
            <React.Fragment>
                { this.props.is_feedback_success ? 
                   <div className="alert alert-primary alert-outline-coloured alert-dismissible" role="alert">
                        <div className="alert-icon">
                            <i className="fe fe-bell"></i>
                        </div>
                        <div className="alert-message">
                            <strong>Congratulations!</strong> Job Application successfull!
                        </div>
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    :
                    <div className="alert alert-danger alert-outline-coloured alert-dismissible" role="alert">
                        <div className="alert-icon">
                            <i className="fe fe-bell"></i>
                        </div>
                        <div className="alert-message">
                            <strong>Failed!</strong> Job Application failed!, Please attach application letter
                        </div>
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                }
            </React.Fragment>

        )
    }
}
