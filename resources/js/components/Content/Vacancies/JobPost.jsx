import React, { Component } from 'react'
import Vacancy from './Vacancy'
import MoreDetails from './MoreDetails'
import Apply from './Apply'

export default class JobPost extends Component {
    componentDidMount(){
      //  console.log('i got here')
    }
  render() {
    return (
      <div className="row">
        <div className="col-12">
            <div className="card mb-2">
                <div className="card-header">
                    <ul className="nav nav-pills card-header-pills pull-right" role="tablist">
                        <li className="nav-item">
                            <a className="nav-link active" data-toggle="tab" 
                                href={"#tab-4" + this.props.vacancy.id}>Vacancy
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href={"#tab-5" + this.props.vacancy.id}>More Details
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href={"#tab-6" + this.props.vacancy.id}>Apply
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="card-body">
                    <div className="tab-content">
                        <div className="tab-pane fade show active" id={"tab-4" + this.props.vacancy.id} role="tabpanel">
                            <Vacancy vacancy={this.props.vacancy}/>
                        </div>
                        <div className="tab-pane fade" id={"tab-5" + this.props.vacancy.id} role="tabpanel">
                            <MoreDetails  vacancy={this.props.vacancy} />
                        </div>
                        <div className="tab-pane fade" id={"tab-6" + this.props.vacancy.id} role="tabpanel">
                            <Apply vacancy={this.props.vacancy}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    )
  }
}

