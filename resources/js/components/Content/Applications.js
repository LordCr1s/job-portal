import React, { Component } from 'react'
import SideBarBaseTemplate from '../SideBarBaseTemplate'
import GraduateApplications from './Applications/GraduateApplications'
import CompanyApplication from './Applications/CompanyApplication'
import { authHeader } from './../helpers/authHeader'
import { notify } from './Utilities'

export default class Applications extends Component {

    cancelApplication = (cancelled_application, status) => {

        const application = {
            application_id : cancelled_application.id,
            status: status
        }
        fetch('/api/application/cancel', {
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(application) 
        })
        .then((resp) => resp.json())
        .then((data) => {
            if(data.status === 200){
                let applications = [...this.state.student_applications]
                let index = applications.indexOf(cancelled_application)
                applications[index].status = "canceled"
                this.setState({
                    student_applications: applications
                }, () => notify('success', 'Application Canceled', 'success') )

            }else{
                notify('Failed', 'there was an error while canceling the appliction', 'error') 
               }
        })
        .catch(error => console.log(error))
    }
  
    setComponent = (item) => {
        if(this.state.role.name === 'student'){
            return <GraduateApplications key={item.id} application={item} cancelApplication={this.cancelApplication}/>
        }else{
            return <CompanyApplication key={item.id} application={item} cancelApplication={this.cancelApplication}/>
        }
       
    }

  render() {

    const context = {
        heading: "Applications",
        search_placeholder: 'Search applications',
        setComponent: (item) => this.setComponent(item)
    }
    
    return (
        <React.Fragment>
            { this.state.is_fetching ? null :
              this.state.role.name === 'student'?
            <SideBarBaseTemplate context={context} items={this.state.student_applications}/>:

            <SideBarBaseTemplate context={context} items={this.state.company_applications}/>
            }
        </React.Fragment>
    )
  }

  state = {
      student_applications: [],
      company_applications: [],
      role: {},
      is_fetching: true
  }

  componentDidMount()
  {
      this.getUser()
  }

  getUser = () => {
    fetch('api/auth/me',{
        method: 'POST',
        headers: authHeader(),
    })
    .then( res => res.json())
    .then( res => {
        if( res.status == 200 && res.role.name == 'student'){
            
            this.setState({
                student_applications:res.user.applications, 
                role: res.role,
                is_fetching: false
            })
            
        }else{
            this.setState({
                company_applications:res.user.vacancies, 
                role: res.role,
                is_fetching: false
            })
        }
        
    })
}
}
