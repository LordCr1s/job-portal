import React, { Component } from 'react'
import PersonalDetails from './Graduate/PersonalDetails'
import ContactDetails from './Graduate/ContactDetails'
import Languages from './Graduate/Languages'
import Academics from './Graduate/Academics'
import Experience from './Graduate/Experience'
import { authHeader } from './../../helpers/authHeader'

export class GraduatesProfile extends Component {

    state = {
        student_id: 0,
        personal_details: {},
        contact_details: {},
        user:{},
        languages: [],
        academics: [],
        experiences: []
    }

    componentDidMount(){
        let userData = this.props.userData
        this.setState({
            personal_details: userData.studentprofile,
            contact_details: userData.contacts,
            languages: userData.languages,
            academics: userData.academics,
            experiences: userData.experiences,
            user: userData
        })
    }

    
       
  render() {
    return (
        <div className="row">
            <div className="col-12">
                <div className="card mb-2">
                    <div className="card-header">
                        <ul className="nav nav-pills card-header-pills pull-right" role="tablist">
                            <li className="nav-item">
                                <a className="nav-link active" data-toggle="tab" 
                                    href={"#tab-4" + this.state.user.id}>
                                    <i className="align-middle fe fe-user"></i>
                                    <span className="align-middle ml-2">Personal details</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" 
                                    href={"#tab-5" + this.state.user.id}>
                                    <i className="align-middle fe fe-slack"></i>
                                    <span className="align-middle ml-2">Contacts</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" 
                                    href={"#tab-6" + this.state.user.id}>
                                    <i className="align-middle fe fe-globe"></i>
                                    <span className="align-middle ml-2">Language</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" 
                                    href={"#tab-7" + this.state.user.id}>
                                    <i className="align-middle fe fe-award"></i>
                                    <span className="align-middle ml-2">Academics</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" 
                                    href={"#tab-8" + this.state.user.id}>
                                    <i className="align-middle fe fe-wind"></i>
                                    <span className="align-middle ml-2">Experience</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="card-body">
                        <div className="tab-content">
                            <div className="tab-pane fade show active" id={"tab-4" + this.state.user.id} role="tabpanel">
                                <PersonalDetails personal_details={this.state.personal_details}/>
                            </div>
                            <div className="tab-pane fade" id={"tab-5" + this.state.user.id} role="tabpanel">
                                <ContactDetails contact_details={this.state.contact_details} user={this.state.user}/>
                            </div>
                            <div className="tab-pane fade" id={"tab-6" + this.state.user.id} role="tabpanel">
                                <Languages languages={this.state.languages} />
                            </div>
                            <div className="tab-pane fade show" id={"tab-7" + this.state.user.id} role="tabpanel">
                                <Academics academics={this.state.academics} />
                            </div>
                            <div className="tab-pane fade" id={"tab-8" + this.state.user.id} role="tabpanel">
                                <Experience experiences={this.state.experiences} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}