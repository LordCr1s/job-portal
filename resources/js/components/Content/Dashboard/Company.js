import React, { Component } from 'react'
import JobPost from '../Vacancies/JobPost'
import { authHeader } from './../../helpers/authHeader'  
import { notify } from './../Utilities'

export default class Company extends Component {

    constructor(props) {
        super(props)
        this.state = {
            vacancies:[],
            company: {},
            companyLocations: [],
            jobFunctions:[],
            locations: [],
            workingHours: [],
            industries: []
        }
    }

    componentDidMount() {
        this.setState({
            company: this.props.company,
            vacancies: this.props.vacancies
        })
        this.getHelpers()
    }

    postNewJob = event => {
        event.preventDefault()

        const new_job = {
            title: $('#title').val(),
            application_deadline: $('#deadline').val(),
            location:  $('#location').val(),
            body: $('#description').val(),
            position: $('#position').val(),
            function_id: $('#jobfunction').val(),
            industry_id: $('#industry').val(),
            location_id: $('#location').val(),
            working_hour_id: $('#workinghour').val(),
        }

        fetch('/api/vacancy/store', {
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(new_job) 
        })
        .then((resp) => resp.json())
        .then((data) => {
            if(data.status === 200){
                notify('success', 'Vacancy Added', 'success')
            }else{ 
                notify('Failed', 'error occured while your your vacancy', 'error')
            }
        })
        .catch(error => console.log(error))
        
    }
    getHelpers = ()=>{
        fetch('/api/vacancy/formhelper',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status === 200)
            this.setState({
                locations: res.locations,
                jobFunctions:res.jobfunctions,
                workingHours: res.workinghours,
                industries: res.industries
            })
          
        }).catch(error => console.log(error))
    }



  render() {
    return (
      <React.Fragment>
        <div className="card mb-2">
            <div className="card-body p-3">
                <h5 className="card-title mb-2 mt-1 text-primary" style={{fontSize: 1.7 + 'em'}}>
                 {this.state.company.company_name} 
                </h5>
                <div className="row">
                    <div className="col-12 col-md-7">
                        <p className="mb-2"> About <span className="text-primary">{this.state.company.company_name}</span> </p>
                        <p className="text-small">
                           {this.state.company.about}
                        </p>
                        <p className="card-title mb-2 mt-1 text-dark" style={{fontSize: 1.3 + 'em'}}>
                            Jobs From this company 
                            <button type="submit" className="btn btn-primary ml-5" 
                                data-toggle="modal" data-target={"#jobModal" + this.state.company.id}>
                                Post new Job
                            </button>
                        </p>
                        <div className="modal fade" id ={"jobModal" + this.state.company.id} tabIndex="-1" role="dialog" style={{display: 'none'}} aria-hidden="true">
                                <div className="modal-dialog modal-lg" role="document">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title"> {this.state.company.company_name}'s new job post</h5>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div className="modal-body m-3">

                                            <form encType="multipart/form-data">
                                                <div className="form-group row">
                                                    <label className="col-form-label col-sm-2 text-sm-right">Job title</label>
                                                    <div className="col-sm-6">
                                                        <input type="text" className="form-control" id="title" 
                                                        name="validation-required" placeholder="job title" >
                                                        </input>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-form-label col-sm-2 text-sm-right">Deadline</label>
                                                    <div className="col-sm-6">
                                                        <input type="text" className="form-control" id="deadline" 
                                                        name="validation-required" placeholder="application deadline" >
                                                        </input>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-form-label col-sm-2 text-sm-right">Location</label>
                                                    <div className="col-sm-6">
                                                    <select className="form-control" id="location">
                                                        <option selected disabled> Select location for the job</option>
                                                        { this.state.locations.map(location =>(
                                                            <option value={location.id}>{location.name}</option>
                                                        ))}												
                                                    </select>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                <label className="col-form-label col-sm-2 text-sm-right">Industry</label>
                                                <div className="col-sm-6">
                                                    <select className="form-control" id="industry">
                                                    <option selected disabled> Select industry for the job</option>
                                                        { this.state.industries.map(industry =>(
                                                            <option value={industry.id}>{industry.name}</option>
                                                        ))}												
                                                    </select>
                                                </div>
                                                </div>
                                                <div className="form-group row">
                                                <label className="col-form-label col-sm-2 text-sm-right">Job Function</label>
                                                <div className="col-sm-6">
                                                    <select className="form-control" id="jobfunction">
                                                    <option selected disabled> Select job function for the job</option>
                                                        { this.state.jobFunctions.map(jobFunction =>(
                                                            <option value={jobFunction.id}>{jobFunction.name}</option>
                                                        ))}												
                                                    </select>
                                                </div>
                                                </div>
                                                <div className="form-group row">
                                                <label className="col-form-label col-sm-2 text-sm-right">Working Hour</label>
                                                <div className="col-sm-6">
                                                    <select className="form-control" id="workinghour">
                                                    <option selected disabled> Select working hour for the job</option>
                                                        { this.state.workingHours.map(workingHour =>(
                                                            <option value={workingHour.id}>{workingHour.name}</option>
                                                        ))}												
                                                    </select>
                                                </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-form-label col-sm-2 text-sm-right">Position</label>
                                                    <div className="col-sm-6">
                                                        <input type="text" className="form-control" id="position" 
                                                        name="validation-required" placeholder="job's Position" >
                                                        </input>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-form-label col-sm-2 text-sm-right">Description</label>
                                                    <div className="col-sm-6">
                                                        <textarea type="text" className="form-control" id="description" 
                                                        name="validation-required" placeholder="job's description" >
                                                        </textarea>
                                                    </div>
                                                </div>
                                                
                                                <div className="form-group row">
                                                    <div className="col-sm-10 ml-sm-auto">
                                                        <button type="submit" className="btn btn-primary" 
                                                        onClick={event => this.postNewJob(event)}>
                                                            Post
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                    <div className="col-12 col-md-5">
                        <h6 className="text-dark"> 
                            Active job posts : <span className="badge badge-secondary ml-2">{this.state.vacancies.length}</span> 
                        </h6>
                        <h6 className="text-dark"> 
                            Locations: { this.state.company.country}
                            {/* {this.state.companyLocations.map(location => 
                                        <span className="badge badge-secondary ml-2" key={location}>
                                            {location}
                                        </span>)} */}
                        </h6> 
                        <h6 className="text-dark"> 
                            Website : 
                            <span className="badge badge-secondary ml-2">
                                <a href={this.state.company.website} className="text-white">{this.state.company.website}</a>
                            </span> 
                        </h6>
                        <h6 className="text-dark"> 
                            Address : <span className="badge badge-secondary ml-2">{this.state.company.address}</span> 
                        </h6>
                        <h6 className="text-dark"> 
                            Email : <span className="badge badge-secondary ml-2">{this.state.company.email_for_notification}</span> 
                        </h6>
                        <h6 className="text-dark"> 
                            Phone : <span className="badge badge-secondary ml-2">{this.state.company.phone_number}</span> 
                        </h6>
                        <h6 className="text-dark"> 
                            Industry : <span className="badge badge-secondary ml-2">{this.state.company.industry}</span> 
                        </h6>
                    </div>
                </div>
            </div>
        </div>

        { this.state.vacancies.map(vacancy => <JobPost key={vacancy.id} vacancy={vacancy}/>)}
      </React.Fragment>
    )
  }
}

