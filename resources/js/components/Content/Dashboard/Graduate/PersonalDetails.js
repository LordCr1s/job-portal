import React, { Component } from 'react'

export default class PersonalDetails extends Component {
  render() {
    return (
        <React.Fragment>
            <div className="card">
                <div className="card-body pt-0 pb-0">
                    <div className="row mb-2">
                        <div className="col-2"> Gender</div>
                        <div className="col-6">: &nbsp; {(this.props.personal_details.gender === null) ? null: this.props.personal_details.gender}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Date of Birth </div>
                        <div className="col-6">: &nbsp; {(this.props.personal_details.date_of_birth == null) ? null: this.props.personal_details.date_of_birth}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Place of Birth </div>
                        <div className="col-6">: &nbsp; {this.props.personal_details.place_of_birth == null ? null: this.props.personal_details.place_of_birth}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Nationality </div>
                        <div className="col-6">: &nbsp; {this.props.personal_details.nationality == null ? null: this.props.personal_details.nationality}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Marital Status </div>
                        <div className="col-6">: &nbsp; {this.props.personal_details.marital_status == null ? null: this.props.personal_details.marital_status}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Phone Number </div>
                        <div className="col-6">: &nbsp; {this.props.personal_details.mobile_phone == null ? null: this.props.personal_details.mobile_phone}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Disability </div>
                        <div className="col-6">: &nbsp; {this.props.personal_details.disability == null ? null: this.props.personal_details.disability}</div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
  }
}
