import React, { Component } from 'react'

export default class Experience extends Component {
  render() {
    return (
        <div className="card">
            <table className="table">
                <tbody>
                    <tr style={{ borderBottom: 'thin solid rgb(219, 225, 230)'}}>
                        <td>Place</td>
                        <td>Job Title</td>
                        <td>Job duration</td>
                    </tr>
                    {this.props.experiences == undefined? <tr></tr>:this.props.experiences.map(experience => (
                        <tr key={experience.id}>
                            <td className="text-muted" style={{ border: 'none'}}>{experience.place}</td>
                            <td className="text-muted" style={{ border: 'none'}}>{experience.job_title}</td>
                            <td className="text-muted" style={{ border: 'none'}}>{experience.job_duration}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
  }
}