import React, { Component } from 'react'

export default class Languages extends Component {
  render() {
    return (
        <div className="card">
            <table className="table">
                <tbody>
                    <tr style={{ borderBottom: 'thin solid rgb(219, 225, 230)'}}>
                        <td>Language</td>
                        <td>Speaking</td>
                        <td>Writting</td>
                        <td>Reading</td>
                    </tr>
                    {this.props.languages.map(language => (
                        <tr key={language.id}>
                             <td className="text-muted" style={{ border: 'none'}}>{language.language}</td>
                             <td className="text-muted" style={{ border: 'none'}}>{language.speaking}</td>
                             <td className="text-muted" style={{ border: 'none'}}>{language.writting}</td>
                             <td className="text-muted" style={{ border: 'none'}}>{language.reading}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
  }
}
