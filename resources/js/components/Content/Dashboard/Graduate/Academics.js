import React, { Component } from 'react'

export default class Academics extends Component {
  render() {
    return (
        <div className="card">
            <table className="table">
                <tbody>
                    <tr style={{ borderBottom: 'thin solid rgb(219, 225, 230)'}}>
                        <td>Level</td>
                        <td>Programe</td>
                        <td>Institution</td>
                        <td>Year</td>
                    </tr>
                    {this.props.academics.map(academic => (
                        <tr key={academic.id}>
                            <td className="text-muted" style={{ border: 'none'}}>{academic.level}</td>
                            <td className="text-muted" style={{ border: 'none'}}>{academic.programe}</td>
                            <td className="text-muted" style={{ border: 'none'}}>{academic.institution}</td>
                            <td className="text-muted" style={{ border: 'none'}}>{academic.year}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
  }
}
