import React, { Component } from 'react'

export default class ContactDetails extends Component {
  render() {
    return (
        <React.Fragment>
            <div className="card">
                <div className="card-body pt-0 pb-0">
                    <div className="row mb-2">
                        <div className="col-2"> Present Address</div>
                        <div className="col-6">: &nbsp; {this.props.contact_details == null ? '': this.props.contact_details.present_address}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Permenet Address </div>
                        <div className="col-6">: &nbsp; {this.props.contact_details == null ? '':this.props.contact_details.permenent_address}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Country </div>
                        <div className="col-6">: &nbsp; {this.props.contact_details == null ? '':this.props.contact_details.country}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> City </div>
                        <div className="col-6">: &nbsp; {this.props.contact_details == null ? '':this.props.contact_details.city}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Province </div>
                        <div className="col-6">: &nbsp; {this.props.contact_details == null ? '':this.props.contact_details.province}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Telephone </div>
                        <div className="col-6">: &nbsp; {this.props.contact_details == null ? '':this.props.contact_details.telephone}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Mobile </div>
                        <div className="col-6">: &nbsp; {this.props.contact_details == null ? '':this.props.contact_details.mobile}</div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-2"> Email </div>
                        <div className="col-6">: &nbsp; {this.props.contact_details == null ? '':this.props.user.email}</div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
  }
}
