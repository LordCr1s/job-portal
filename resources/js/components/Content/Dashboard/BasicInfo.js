import React, { Component } from 'react'
import { authHeader } from './../../helpers/authHeader'

export default class BasicInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            personal_details:{},
            contact_details:{},
            user:{},
            academics:[],
        }
    }
    componentDidMount() {
        let userData = this.props.userData
        this.setState({
            personal_details: userData.studentprofile,
            academics: userData.academics.reverse(),
            user: userData
        })
    }

   
  render() {
    let user_academics = this.state.academics[0]
    return (
        <div className="card mb-2">
            <div className="card-body p-2">
                <div className="row"> 
                    <div className="col-auto">
                        <div className="text-left">
                            <img className="rounded mt-2 ml-2" src={"http://127.0.0.1:8000/" + this.state.personal_details.image } alt="Placeholder" width="110" height="110"/>
                        </div>
                    </div>
                    <div className="col-8 p-1 ml-2">
                        <h2 className=" mb-1 mt-1 text-primary"> {(this.state.personal_details.first_name === null) ? null: this.state.personal_details.first_name} {(this.state.personal_details.last_name === null )? null: this.state.personal_details.last_name}</h2>
                        <h6 className="text-dark mb-1"> 
                            <span className="text-muted ml-1">{ (user_academics != undefined) ? user_academics.institution : null}</span> 
                        </h6>
                        <h6 className="text-muted mb-1"> 
                            <span className="text-muted ml-1">{ (user_academics != undefined) ? user_academics.programe : null}</span> 
                        </h6>
                        <h6 className="text-muted mb-1"> 
                            <span className="text-muted ml-1">{this.state.user.email == null ?null:this.state.user.email}</span> 
                        </h6>
                        <h6 className="text-muted mb-1 ml-1"> 
                            <span className="fe fe-award text-success">
                                <span className="ml-2" style={{fontFamily: 'cerebrisans-regular'}}>{ (user_academics != undefined) ? user_academics.year : null}</span>
                            </span>
                        </h6>
                    </div>
                    {/* <div className="col-auto">
                        <div><span className="text-muted"> Profile Completed by </span> </div>
                        
                        <div data-label="40%" className="doughnut mt-3 ml-3 doughnut-success doughnut-40"></div>
                    </div> */}
                </div>
            </div>
        </div>
    )
  }
}
