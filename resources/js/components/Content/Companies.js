import React, { Component } from 'react'
import Company from './company/company'
import CompanyDetails from './company/companyDetails'
import SideBarBaseTemplate from '../SideBarBaseTemplate'
import { authHeader } from '../helpers/authHeader'

export const company = () => <Company />
export const CompanyDetail = (props) => <CompanyDetails match={props.match} location={props.location} />

export default class Companies extends Component {

  render() {

    const context = {
        heading: "Companies",
        search_placeholder: 'Search companies', 
        setComponent: (item) => <Company key={item.id} company={item}/>
    }

    return (
        <React.Fragment>
            {
                (this.state.companies.length == 0) ? null:
                <SideBarBaseTemplate context={context} items={this.state.companies}/>
            }
        </React.Fragment>
    )
  }

  state = {
      companies: []
  }

    componentDidMount() {
        // perform fetch here
    this.getCompanies()

    }

    getCompanies = () => {
        fetch('/api/company/home',{
            method: 'GET',
            headers: authHeader()
        }).then(response => response.json().then( response => 
            this.setState({
                companies: response.companyprofile
            })
        ))
    }
}


