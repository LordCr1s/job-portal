import React, { Component } from 'react'
import JobPost from './Vacancies/JobPost'
import SideBarBaseTemplate from '../SideBarBaseTemplate'
import { authHeader } from '../helpers/authHeader'

export default class Vacancies extends Component {


  render() {

    const context = {
        heading: "Job Alerts",
        search_placeholder: 'Search job alerts',
        setComponent: (item) => <JobPost key={item.id} vacancy={item}/>
    }

    return (
        <React.Fragment>
            {
                (this.state.vacancies.length == 0) ? null:
                <SideBarBaseTemplate context={context} items={this.state.vacancies}/>
            }
        </React.Fragment>
    )
  }

  state = {
     vacancies: [],
     
  }

  componentDidMount() {
    // perform fetch here
    this.getVacances()

}

getVacances = () => {
    fetch('/api/vacancy/home',{
        method: 'GET',
        headers: authHeader()
    })
    .then( res => res.json())
    .then( res => {
        this.setState({
            vacancies: res.student_vacancies
        })
      
    })
}
}
