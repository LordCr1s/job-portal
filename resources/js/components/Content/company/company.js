import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Company extends Component {
  render() {
    return (
        <div className="card mb-2">
            <div className="card-body p-3">
                <h5 className="card-title mb-3 mt-1 text-primary"> {this.props.company.company_name} </h5>
                <h6 className="text-muted"> 
                    <span className="fe fe-radio text-warning mr-1"></span>Active job posts : 
                    <span className="text-secondary ml-2">{this.props.company.user.vacancies.length}</span> 
                </h6>
                <h6 className="text-muted"> 
                    <span className="fe fe-map-pin text-warning mr-1"></span>Locations
                    <span className="text-secondary ml-2" > {this.props.company.country}</span>  
                    {/* {this.props.company.locations.map(location => <span className="text-secondary ml-2" key={location}>{location}</span>)} */}
                </h6> 
                <div className="pt-1">
                    <Link to={{
                        pathname:`companies/${this.props.company.id}`, 
                        state: {company: this.props.company},
                        search: '?omakei'
                        }}>
                        <span className="fe fe-eye text-primary">
                            <span className="ml-2" style={{fontFamily: 'cerebrisans-regular', textDecoration: 'underline'}}>See more </span>
                        </span>
                    </Link>
                </div>
            </div>
        </div>
    )
  }
}
