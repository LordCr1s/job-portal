import React, { Component } from 'react'
import JobPost from '../Vacancies/JobPost'
import { authHeader } from '../../helpers/authHeader'

export default class CompanyDetails extends Component {

    constructor(props) {
        super(props)
        this.state = {
            vacancies:[],
            company: {},
            companyLocations: []
        }
    }

    componentDidMount() {
        this.setState({
            company: this.props.location.state.company,
            vacancies: this.props.location.state.company.user.vacancies
        })
    }


  render() {
    return (
      <React.Fragment>
        <div className="card mb-2">
            <div className="card-body p-3">
                <h5 className="card-title mb-2 mt-1 text-primary" style={{fontSize: 1.7 + 'em'}}>
                 {this.state.company.company_name} 
                </h5>
                <div className="row">
                    <div className="col-12 col-md-7">
                        <p className="mb-2"> About <span className="text-primary">{this.state.company.company_name}</span> </p>
                        <p className="text-small">
                           {this.state.company.about}
                        </p>
                        <p className="card-title mb-2 mt-1 text-dark" style={{fontSize: 1.3 + 'em'}}>
                            Jobs From this company 
                        </p>
                    </div>
                    <div className="col-12 col-md-5">
                        <h6 className="text-dark"> 
                            Active job posts : <span className="badge badge-secondary ml-2">{this.state.company.user | null}</span> 
                        </h6>
                        <h6 className="text-dark"> 
                            Locations: { this.state.company.country}
                            {/* {this.state.companyLocations.map(location => 
                                        <span className="badge badge-secondary ml-2" key={location}>
                                            {location}
                                        </span>)} */}
                        </h6> 
                        <h6 className="text-dark"> 
                            Website : 
                            <span className="badge badge-secondary ml-2">
                                <a href={this.state.company.website} className="text-white">{this.state.company.website}</a>
                            </span> 
                        </h6>
                        <h6 className="text-dark"> 
                            Address : <span className="badge badge-secondary ml-2">{this.state.company.address}</span> 
                        </h6>
                        <h6 className="text-dark"> 
                            Email : <span className="badge badge-secondary ml-2">{this.state.company.email_for_notification}</span> 
                        </h6>
                        <h6 className="text-dark"> 
                            Phone : <span className="badge badge-secondary ml-2">{this.state.company.phone_number}</span> 
                        </h6>
                        <h6 className="text-dark"> 
                            Industry : <span className="badge badge-secondary ml-2">{this.state.company.industry}</span> 
                        </h6>
                    </div>
                </div>
            </div>
        </div>

        { this.state.vacancies.map(vacancy => <JobPost key={vacancy.id} vacancy={vacancy}/>)}
      </React.Fragment>
    )
  }
}

