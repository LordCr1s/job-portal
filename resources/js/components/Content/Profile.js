import React, { Component } from 'react';
import Editprofile from './Profile/Student/Editprofile';
import {authHeader} from './../helpers/authHeader'

export default class Profile extends Component {

  state = {
    user: {},
    role:{},
    isFetching: true,
    company: {},
    vacancies: [],
    is_edit_mode: false
  }

  changeViewMode = mode => {
    let state = {...this.state}
    state.is_edit_mode = mode
    this.setState(state)
  }

  componentDidMount() {
    this.getUser()

  }

  getUser = async () => {
    await fetch('api/auth/me',{
        method: 'POST',
        headers: authHeader()
    })
    .then( res => res.json())
    .then( res => {
        if( res.status == 200 && res.role.name === 'student'){
          this.setState({
            user: res.user,
            role: res.role,
            isFetching: false
          })
        }else if(res.status == 200 && res.role.name === 'company') {
          this.setState({
            user: res.user,
            role: res.role,
            company: res.user.companyprofile,
            vacancies: res.user.vacancies,
            isFetching: false
          })
        }
    })
  }

  render() {
    return (
      <div className="row">
					<div className="col-12">
            {this.state.isFetching == false? <Editprofile showProfile={this.changeViewMode} role={this.state.role} />:null}
            
					</div>
			</div>
    )
  }
}
