import React, { Component } from 'react'
import Graduate from './Graduates/Graduate'
import SideBarBaseTemplate from '../SideBarBaseTemplate'
import { authHeader } from '../helpers/authHeader'

export default class Students extends Component {

  render() {

    const context = {
        heading: "Graduates",
        search_placeholder: 'Search graduates',
        setComponent: (item) => <Graduate key={item.id} graduate={item} image={this.state.image}/>
    }

    return (
        <React.Fragment>
            {
                (this.state.graduates.length == 0) ? null:
                <SideBarBaseTemplate context={context} items={this.state.graduates}/>
            }
        </React.Fragment>
    )
  }

  state = {
       graduates: [
    //       {
    //           id: 2,
    //           name: 'Mansoor Rashid Mansoor',
    //           insitute: 'University of Dodoma',
    //           qualification: 'Bachelor of science in Mechanical Engineering',
    //           year: 2016,
    //           image: "img/mansoor.jpg"
    //       },
      ]
  }

  componentDidMount() {
    // perform fetch here
    this.getGraduets()

}

getGraduets = () => {
    fetch('api/student/home',{
        method: 'GET',
        headers: authHeader()
    })
    .then( res => res.json())
    .then( res => {
        this.setState({
            graduates: res.students,
            image: "img/omakei.jpg"
        })
       // console.log(res.students)
    })
}
}
