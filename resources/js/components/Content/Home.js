import React, { Component } from 'react'
import BasicInfo from './Dashboard/BasicInfo'
import { GraduatesProfile } from './Dashboard/Profile'
import { authHeader } from '../helpers/authHeader'
import { Redirect } from 'react-router-dom'
import Company from './Dashboard/Company';

export default class Home extends Component {

  state = {
    user: {},
    role:{},
    isFetching: true,
    company: {},
    vacancies: []
  }

  componentDidMount() {
    this.getUser()

  }

  getUser = async () => {
    await fetch('api/auth/me',{
        method: 'POST',
        headers: authHeader()
    })
    .then( res => res.json())
    .then( res => {
        if( res.status == 200 && res.role.name === 'student'){
          this.setState({
            user: res.user,
            role: res.role,
            isFetching: false
          })
        }else if(res.status == 200 && res.role.name === 'company') {
          this.setState({
            user: res.user,
            role: res.role,
            company: res.user.companyprofile,
            vacancies: res.user.vacancies,
            isFetching: false
          })
        }
    })
  }

  render() {
    return (
      (!!localStorage.getItem('access_token') && !!localStorage.getItem('user')) ?  
      <React.Fragment>
        { 
          this.state.role.name === 'student'? 
          
            this.state.isFetching ? null : 
              <React.Fragment>
                <BasicInfo userData={this.state.user}/>
                <GraduatesProfile userData={this.state.user}/>
              </React.Fragment>
          :
          this.state.isFetching ? null : 
              <React.Fragment>
                 <Company company={this.state.company} vacancies={this.state.vacancies}/>
              </React.Fragment>
        
        }
      </React.Fragment> : <Redirect to='login'/>

    )
  }
}
