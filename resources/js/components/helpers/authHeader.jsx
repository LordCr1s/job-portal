export function authHeader() {
    // return authorization header with jwt token
    // let user = JSON.parse(localStorage.getItem('user'));

    // if (user && user.token) {
    //     return { 'Authorization': 'Bearer ' + user.token };
    // } else {
    //     return {};
    // }

    return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization' : 'Bearer ' + localStorage.getItem('access_token'),
    }
}

export function authHeaderUpload() {
    // return authorization header with jwt token
    // let user = JSON.parse(localStorage.getItem('user'));

    // if (user && user.token) {
    //     return { 'Authorization': 'Bearer ' + user.token };
    // } else {
    //     return {};
    // }

    return {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization' : 'Bearer ' + localStorage.getItem('access_token'),
    }
}