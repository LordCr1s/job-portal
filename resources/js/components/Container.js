import React, { Component } from 'react';
import Topnav from './Topnav';
import Sidenav from './Sidenav';
import Content from './Content';
import Login from './Auth/Login'


export default class Container extends Component {
  render() {
    return (
        <React.Fragment>
          { (localStorage.getItem('access_token') && localStorage.getItem('user')) ? 
           <div className="d-flex">
                <Sidenav />
                <div className="main">
                  <Topnav />
                  <main className="content">
                    <div className="container-fluid p-0">
                      <Content />
                    </div>
                  </main>
                </div>
            </div>:
            <Login/>
          }
        </React.Fragment>
    )
  }
}
