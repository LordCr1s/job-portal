import { BrowserRouter } from 'react-router-dom';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Container from './Container';
import { jpgs_store } from './Data/index'
import Main from './Auth/Main';
import { Redirect, Link } from 'react-router-dom'
import Login from './Auth/Login';

export default class Root extends Component {

    render() {
        return (
            <BrowserRouter>
                <Container /> 
            </BrowserRouter>
        );
    }
}

const renderJpgs = () => {
    if (document.getElementById('root')) {
        ReactDOM.render(<Root />, document.getElementById('root'));
    }
}
renderJpgs()


