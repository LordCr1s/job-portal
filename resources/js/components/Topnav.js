import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { authHeader } from './helpers/authHeader'


export default class Topnav extends Component {
    state = {
        user:{},
        role:{},
        isFetching: true,
        redirect: false
      }

    componentDidMount() {
        this.getUser()
    
      }
    
      getUser = async () => {
        await fetch('api/auth/me',{
            method: 'POST',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200 && res.role.name === 'student'){
              this.setState({
                user:res.user,
                role: res.role,
                isFetching: false
              })
            }else if(res.status == 200 && res.role.name === 'company') {
              this.setState({
                user: res.user,
                role: res.role,
                isFetching: false
              })
            }
        })
      }

      logout = (e) =>{
        event.preventDefault()
        fetch('api/auth/logout',{
            method: 'POST',
            headers: authHeader()
        }).then(res => res.json())
        .then(data => {
            localStorage.clear();
            this.setState({
                redirect:true
            })
        })
      }
      
  render() {
    if(this.state.redirect){
      return  <Redirect to='/'/>
    } else{

    
    return (
        (!!localStorage.getItem('access_token') && !!localStorage.getItem('user')) ?
        
        <React.Fragment>
            <nav className="navbar navbar-expand navbar-light bg-white" id="topnavbar">
                <a className="sidebar-toggle d-flex mr-2">
                    <i className="hamburger align-self-center"></i>
                </a>
                <Link to='/' className="sidebar-brand navbar-brand">
                        <i className="align-middle text-dark" data-feather="box"></i>
                        <span className="align-middle">JPGS</span>
                </Link>
                <div className="navbar-collapse collapse">
                    <ul className="navbar-nav ml-auto"> 
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="userDropdown" data-toggle="dropdown">
                                <span className="d-inline-block d-md-none">
                                    <i className="align-middle" data-feather="settings"></i>
                                </span>
                                { this.state.role.name === 'student'?
                                <React.Fragment>
                                    <span className="d-none d-sm-inline-block">
                                    <img src={this.state.user.studentprofile.image} className="avatar img-fluid rounded-circle mr-1" alt="JPGS" />
                                    <span className="text-dark">{(this.state.user.studentprofile == null)? null: this.state.user.studentprofile.first_name } {(this.state.user.studentprofile.last_name === null)? null: this.state.user.studentprofile.last_name}</span>
                                    </span>
                                </React.Fragment>
                                   :
                                   <React.Fragment>
                                        <span className="d-none d-sm-inline-block">
                                        <img src="/storage/defaults/user.png" className="avatar img-fluid rounded-circle mr-1" alt="image" />
                                        <span className="text-dark">{this.state.companyprofile == undefined? null: this.state.companyprofile.company_name}</span>
                                        </span>
                                   </React.Fragment>
                               
                                }
                                
                            </a>
                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                              
                                <button  className="dropdown-item btn-transparent"  onClick={() => this.logout()} >Sign out</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </React.Fragment> : null
    )}
  }
}
