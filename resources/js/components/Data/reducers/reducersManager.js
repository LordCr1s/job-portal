import { combineReducers } from 'redux'
import { studentReducer } from './studentReducer'

export const reducers_manager = combineReducers({
    student_provider: studentReducer,
});