/**
 * This reducer manages the student's profile editor, profile editor has six sections 
 * PERSONAL DETAILS, CONTACTS, LANGUAGE, ACADEMICS, ATTACHMENTS, EXPERIENCE
 * the code will be devided into parts according to these sections even though they will share
 * there will be one reducer function and one state, the rest will be put to reflect the section
 * of the profile editor
 * REFER TO THE STUDENT PROFILE EDITOR WIZARD ON THE FRONTEND TO UNDERSTAND THIS
 */


// generic way to perform create, update and delete operations 
// on language, academics, attachmets and experience section
const changeState = {
    academics: (state, new_items) => {
        let newState = {
            ...state,
            academic_records: new_items
        }
        return newState
    },
    languages: (state, new_items) => {
        let newState = {
            ...state,
            languages: new_items
        }
        return newState
    },
    other_attachments: (state, new_items) => {
        let newState = {
            ...state,
            other_attachments: new_items
        }
        return newState
    },
    experiences: (state, new_items) => {
        let newState = {
            ...state,
            experiences: new_items
        }
        return newState
    }
}

const createUpdateDelete = (params = {}) => {
    switch(params.operation) {
        case 'CREATE':
            let new_items = [...params.items]
            new_items.push(params.data)
            return changeState[params.type](params.state, new_items)
        case 'UPDATE':
            let updated_items = [...params.items]
            updated_items[params.index] = params.data
            return changeState[params.type](params.state, updated_items)
        case 'DELETE':
            let items_after_delete = params.items.filter(item => item.id != data.id)
            return changeState[params.type](params.state, items_after_delete)
    }
}


 //###################  CONCTACTS SECTION

const ADD_OR_UPDATE_CONTACT_INFORMATION = 'contact:newOrUpdatedInformation'

const contactsActionsCreator = (action, information, section) => ({
    type: action,
    payload: {
        information: information,
        section: section
    }
})

const addOrUpdateContact = (information, section) => contactsActionsCreator(ADD_OR_UPDATE_CONTACT_INFORMATION, information, section)

// ################ PERSONAL DETAILS

const ADD_OR_UPDATE_PERSONAL_DETAILS = 'personaldetails:crud'

const personalDetailsActionsCreator = (action, details) => ({
    type: action,
    payload: {
        details: details,
    }
})

const addUpdatePersonalDetail = details => personalDetailsActionsCreator(ADD_OR_UPDATE_PERSONAL_DETAILS, details)

 //###################  LANGUAGE SECTION

 const DELETE_LANGUAGE ='language:delete'
 const CREATE_LANGUAGE = 'language:create'
 const UPDATE_LANGUAGE = 'language:update'
 
 const LanguageActionsCreator = (language, action, index) => ({
     type: action,
     payload: {
         language: language,
         index: index
     }
 })
 
 const deleteLanguage = language => LanguageActionsCreator(language, DELETE_LANGUAGE)
 const createLanguage = language => LanguageActionsCreator(language, CREATE_LANGUAGE)
 const updateLanguage = (language, index) => LanguageActionsCreator(language, UPDATE_LANGUAGE, index)


// ##################   ACADEMICS SECTION

const CREATE_ACADEMIC_RECORD = 'academics:create'
const UPDATE_ACADEMIC_RECORD = 'academics:update'
const DELETE_ACADEMIC_RECORD = 'academics:delete'

const academicsActionsCreator = (action, record, index) => ({
    type: action,
    payload: {
        record: record,
        index: index
    }
})

const createAcademicRecord = record => academicsActionsCreator(CREATE_ACADEMIC_RECORD, record)
const updateAcademicRecord = (record, index) => academicsActionsCreator(UPDATE_ACADEMIC_RECORD, record, index)
const deleteAcademicRecord = record => academicsActionsCreator(DELETE_ACADEMIC_RECORD, record)

// ##################   ATTACHMENTS SECTION

const CREATE_OTHER_ATTACHMENT = 'otherAttachment:create'
const UPDATE_OTHER_ATTACHMENT = 'otherAttachment:update'
const DELETE_OTHER_ATTACHMENT = 'otherAttachment:delete'

const attachmentActionsCreator = (action, attachment, index) => ({
    type: action,
    payload: {
        attachment: attachment,
        index: index,
    }
})

const createOtherAttachment = attachment => attachmentActionsCreator(CREATE_OTHER_ATTACHMENT, attachment)
const updateOtherAttachment = (attachment, index) => attachmentActionsCreator(UPDATE_OTHER_ATTACHMENT, attachment, index)
const deleteOtherAttachment = attachment => attachmentActionsCreator(DELETE_OTHER_ATTACHMENT, attachment)

// ##################   EXPERIENCE SECTION

const CREATE_EXPERIENCE = 'experience:create'
const UPDATE_EXPERIENCE = 'experience:update'
const DELETE_EXPERIENCE = 'experience:delete'

const experienceActionsCreator = (action, experience, index) => ({
    type: action,
    payload: {
        experience: experience,
        index: index,
    }
})

const createExperience = experience => experienceActionsCreator(CREATE_EXPERIENCE, experience)
const updateExperience = (experience, index) => experienceActionsCreator(UPDATE_EXPERIENCE, experience, index)
const deleteExperience = experience => experienceActionsCreator(DELETE_EXPERIENCE, experience)

export const studentReducer = (state = studentProfile, {
    type,
    payload
}) => {
    switch (type) {
        
        //###############   CONTACT INFORMATION
        // same action will add new contact information and update it
        case ADD_OR_UPDATE_CONTACT_INFORMATION:
            let contact_information = {...state.contact_information}
            contact_information = payload.information
            let newContactInformation = {
                ...state,
                contact_information: contact_information
            }
            return newContactInformation

        //###############   CONTACT INFORMATION
        // same action will add new contact information and update it
        case ADD_OR_UPDATE_PERSONAL_DETAILS:
            let personal_details = {...state.personal_details}
            personal_details = payload.details
            let newPersonalDetails = {
                ...state,
                personal_details: personal_details
            }
            return newPersonalDetails


        //###############   LANGUAGE
        case CREATE_LANGUAGE:
            return createUpdateDelete({
                operation: 'CREATE',
                state: state,
                data: payload.language,
                items: state.languages,
                type: 'languages'
            })
        case UPDATE_LANGUAGE:
            return createUpdateDelete({
                operation: 'UPDATE',
                state: state,
                data: payload.language,
                items: state.languages,
                type: 'languages',
                index: payload.index
            })
        case DELETE_LANGUAGE:
            let languages_after_deleting = state.languages.filter(lang => lang.id != payload.language.id)
            return changeState['languages'](state, languages_after_deleting)

        //###############   ACADEMICS
        case CREATE_ACADEMIC_RECORD:
            return createUpdateDelete({
                operation: 'CREATE',
                state: state,
                data: payload.record,
                items: state.academic_records,
                type: 'academics'
            })
        case UPDATE_ACADEMIC_RECORD:
            return createUpdateDelete({
                operation: 'UPDATE',
                state: state,
                data: payload.record,
                items: state.academic_records,
                type: 'academics',
                index: payload.index
            })
        case DELETE_ACADEMIC_RECORD:
            let records_after_delete = state.academic_records.filter(record => record.id != payload.record.id)
            return changeState['academics'](state, records_after_delete)

        //###############   ATTACHMENTS
        case CREATE_OTHER_ATTACHMENT:
            return createUpdateDelete({
                operation: 'CREATE',
                state: state,
                data: payload.attachment,
                items: state.other_attachments,
                type: 'other_attachments'
            })
        case UPDATE_OTHER_ATTACHMENT:
            return createUpdateDelete({
                operation: 'UPDATE',
                state: state,
                data: payload.attachment,
                items: state.other_attachments,
                type: 'other_attachments',
                index: payload.index
            })
        case DELETE_OTHER_ATTACHMENT:
            let other_attachments_after_delete = state.other_attachments.filter(attachment => attachment.id != payload.attachment.id)
            return changeState['other_attachments'](state, other_attachments_after_delete)


        //###############   EXPERIENCES
        case CREATE_EXPERIENCE:
            return createUpdateDelete({
                operation: 'CREATE',
                state: state,
                data: payload.experience,
                items: state.experiences,
                type: 'experiences'
            })
        case UPDATE_EXPERIENCE:
            return createUpdateDelete({
                operation: 'UPDATE',
                state: state,
                data: payload.experience,
                items: state.experiences,
                type: 'experiences',
                index: payload.index
            })
        case DELETE_EXPERIENCE:
            let experiences_after_delete = state.experiences.filter(experience => experience.id != payload.experience.id)
            return changeState['experiences'](state, experiences_after_delete)

        default:
            return state
    }
}

const studentProfile = {
    academic_records : [],
    languages: [],
    other_attachments: [],
    experiences : [],
    contact_information: {},
    personal_details: {},
    actions : {
        // language editors 
        deleteLanguage,
        createLanguage,
        updateLanguage,

        // contacts editors 
        addOrUpdateContact,

        // personal details
        addUpdatePersonalDetail,

        // academics editor
        createAcademicRecord,
        updateAcademicRecord,
        deleteAcademicRecord,

        // other attachments editor
        createOtherAttachment,
        updateOtherAttachment,
        deleteOtherAttachment,

        // experience editor
        createExperience,
        updateExperience,
        deleteExperience,
    }
}