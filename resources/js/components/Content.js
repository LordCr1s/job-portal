import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Content/Home'
import Profile from './Content/Profile'
import Vacancies from './Content/Vacancies'
import Applications from './Content/Applications'
import Companies, {CompanyDetail, company} from './Content/Companies'
import Students from './Content/Students'
import Subscriptions from './Content/Subscriptions'
import Settings from './Content/Settings'
import { jpgs_store } from './Data/index'
import { Provider } from 'react-redux'
import NotFound from './404'
import Login from './Auth/Login'
import Register from './Auth/Register'
import Verify from './Auth/Verify'
import ForgetPassword from './Auth/ForgetPassword'
import Activation from './Auth/Activation'
import ResetPassword from './Auth/ResetPassword'
import Container from './Container'


export default class Content extends Component {
  render() {
    return (
        <Provider store={jpgs_store}>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route exact path='/home' component={Home}/>
                <Route exact path='/profile' component={Profile}/>
                <Route exact path='/vacancies' component={Vacancies}/>
                <Route exact path='/applications' component={Applications}/>
                <Route exact path='/students' component={Students}/>
                <Route exact path='/subscriptions' component={Subscriptions}/>
        
                {/* companies routes */}
                <Route exact path='/companies' component={Companies}/>
                <Route path={`/companies/:companyId`} render={ location => <CompanyDetail {...location} />}/>

                {/* authentication routes */}
                <Route exact path='/login' render={ props => <Login {...props} />}/>
                <Route exact path='/register' component={Register}/>
                <Route exact path='/Verify' component={Verify}/>
                <Route exact path='/activate' component={Activation}/>
                <Route exact path='/forgetpassword' component={ForgetPassword}/>
                <Route exact path='/resetpassword' component={ResetPassword}/>

                {/* show 404 if nothing is found */}
                <Route component={NotFound} />
            </Switch>
        </Provider>
    )
  }
}
