-- MySQL dump 10.17  Distrib 10.3.14-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: car_renting
-- ------------------------------------------------------
-- Server version	10.3.14-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts_customuser`
--

DROP TABLE IF EXISTS `accounts_customuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_customuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_customuser`
--

LOCK TABLES `accounts_customuser` WRITE;
/*!40000 ALTER TABLE `accounts_customuser` DISABLE KEYS */;
INSERT INTO `accounts_customuser` VALUES (1,'pbkdf2_sha256$150000$g4JcHXP9L2gM$/zE2ixVHIMCzA6U4HPzJ4u/i6eObSZxDK7Jpy5Y4ilM=','2019-06-13 02:52:07.821285','admin@admin.org','Andrew','Chauka','2019-05-14 13:47:32.000000',1,1,1),(2,'pbkdf2_sha256$150000$DmxBIBNL04kx$V1lNai1SHUjzXDhwAn6dB6DM28+5a9ulwrIUBq+YFF8=','2019-06-13 02:49:02.538073','chirstopher@gmail.com','christopher','shoo','2019-05-14 14:38:12.149337',0,0,1),(4,'pbkdf2_sha256$150000$uAI9s9dFfU43$U8DbJ7KcGvfwfe7cZiG7bJUYu5yHWfymQNi1SGYHP5s=','2019-06-04 15:24:51.628024','eliot@gmail.com','eliot','aldeson','2019-06-04 13:42:39.888761',0,0,1),(5,'pbkdf2_sha256$150000$OHFWwaFjGNuU$cYrkIYTtssf/cLqV+UQsYwO2In7c3PAgo68CVfIxpPc=','2019-06-11 04:16:49.573069','christopherbenson17@gmail.com','Christopher','shoo','2019-06-11 03:53:58.710344',0,0,1),(6,'pbkdf2_sha256$150000$yRB1fnXFT0ll$u90ptH3IreyyN61kfJzpuv0TaH9ehAKxKEH2GdU2Azo=','2019-06-11 04:13:22.482558','chaukajr@gmail.com','chauka','jr','2019-06-11 03:57:29.549763',0,0,1);
/*!40000 ALTER TABLE `accounts_customuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_customuser_groups`
--

DROP TABLE IF EXISTS `accounts_customuser_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_customuser_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customuser_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accounts_customuser_groups_customuser_id_group_id_c074bdcb_uniq` (`customuser_id`,`group_id`),
  KEY `accounts_customuser_groups_group_id_86ba5f9e_fk_auth_group_id` (`group_id`),
  CONSTRAINT `accounts_customuser__customuser_id_bc55088e_fk_accounts_` FOREIGN KEY (`customuser_id`) REFERENCES `accounts_customuser` (`id`),
  CONSTRAINT `accounts_customuser_groups_group_id_86ba5f9e_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_customuser_groups`
--

LOCK TABLES `accounts_customuser_groups` WRITE;
/*!40000 ALTER TABLE `accounts_customuser_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_customuser_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_customuser_user_permissions`
--

DROP TABLE IF EXISTS `accounts_customuser_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_customuser_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customuser_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accounts_customuser_user_customuser_id_permission_9632a709_uniq` (`customuser_id`,`permission_id`),
  KEY `accounts_customuser__permission_id_aea3d0e5_fk_auth_perm` (`permission_id`),
  CONSTRAINT `accounts_customuser__customuser_id_0deaefae_fk_accounts_` FOREIGN KEY (`customuser_id`) REFERENCES `accounts_customuser` (`id`),
  CONSTRAINT `accounts_customuser__permission_id_aea3d0e5_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_customuser_user_permissions`
--

LOCK TABLES `accounts_customuser_user_permissions` WRITE;
/*!40000 ALTER TABLE `accounts_customuser_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_customuser_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_userprofile`
--

DROP TABLE IF EXISTS `accounts_userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_userprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile_contact` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `profile_image` varchar(100) DEFAULT NULL,
  `booked_cars` int(11) NOT NULL,
  `owned_cars` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `accounts_userprofile_user_id_92240672_fk_accounts_customuser_id` FOREIGN KEY (`user_id`) REFERENCES `accounts_customuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_userprofile`
--

LOCK TABLES `accounts_userprofile` WRITE;
/*!40000 ALTER TABLE `accounts_userprofile` DISABLE KEYS */;
INSERT INTO `accounts_userprofile` VALUES (1,'(+255)-738-223-985','Kwa aziziali, Temeke','1996-08-21','users/images/2019/48/14/_E5A3093.jpg',0,0,1),(2,'(+255) 625 890765','changombe','1996-08-09','users/images/2019/07/17/avatar.jpg',1,1,2),(4,'(+255)-738-223-985','Ubungo, Riverside','1994-08-02','users/images/2019/23/04/wallpapersecond.jpg',0,1,4),(5,'(+255)-788-426-816','kariakoo','1995-12-12','users/images/2019/02/11/FC2DE5C9-7EA7-42AA-8946-EBCF05B6B560.jpeg',0,1,5),(6,'(+255) 738 223 985','posta','1978-04-12','users/images/2019/04/11/devonjade-1556440218684-7385.jpg',1,1,6);
/*!40000 ALTER TABLE `accounts_userprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add custom user',1,'add_customuser'),(2,'Can change custom user',1,'change_customuser'),(3,'Can delete custom user',1,'delete_customuser'),(4,'Can view custom user',1,'view_customuser'),(5,'Can add user profile',2,'add_userprofile'),(6,'Can change user profile',2,'change_userprofile'),(7,'Can delete user profile',2,'delete_userprofile'),(8,'Can view user profile',2,'view_userprofile'),(9,'Can add car',3,'add_car'),(10,'Can change car',3,'change_car'),(11,'Can delete car',3,'delete_car'),(12,'Can view car',3,'view_car'),(13,'Can add log entry',4,'add_logentry'),(14,'Can change log entry',4,'change_logentry'),(15,'Can delete log entry',4,'delete_logentry'),(16,'Can view log entry',4,'view_logentry'),(17,'Can add permission',5,'add_permission'),(18,'Can change permission',5,'change_permission'),(19,'Can delete permission',5,'delete_permission'),(20,'Can view permission',5,'view_permission'),(21,'Can add group',6,'add_group'),(22,'Can change group',6,'change_group'),(23,'Can delete group',6,'delete_group'),(24,'Can view group',6,'view_group'),(25,'Can add content type',7,'add_contenttype'),(26,'Can change content type',7,'change_contenttype'),(27,'Can delete content type',7,'delete_contenttype'),(28,'Can view content type',7,'view_contenttype'),(29,'Can add session',8,'add_session'),(30,'Can change session',8,'change_session'),(31,'Can delete session',8,'delete_session'),(32,'Can view session',8,'view_session'),(33,'Can add booking',9,'add_booking'),(34,'Can change booking',9,'change_booking'),(35,'Can delete booking',9,'delete_booking'),(36,'Can view booking',9,'view_booking'),(37,'Can add notification',10,'add_notification'),(38,'Can change notification',10,'change_notification'),(39,'Can delete notification',10,'delete_notification'),(40,'Can view notification',10,'view_notification'),(41,'Can add booking history',11,'add_bookinghistory'),(42,'Can change booking history',11,'change_bookinghistory'),(43,'Can delete booking history',11,'delete_bookinghistory'),(44,'Can view booking history',11,'view_bookinghistory'),(45,'Can add payment record',12,'add_paymentrecord'),(46,'Can change payment record',12,'change_paymentrecord'),(47,'Can delete payment record',12,'delete_paymentrecord'),(48,'Can view payment record',12,'view_paymentrecord'),(49,'Can add car images',13,'add_carimages'),(50,'Can change car images',13,'change_carimages'),(51,'Can delete car images',13,'delete_carimages'),(52,'Can view car images',13,'view_carimages'),(53,'Can add car image',13,'add_carimage'),(54,'Can change car image',13,'change_carimage'),(55,'Can delete car image',13,'delete_carimage'),(56,'Can view car image',13,'view_carimage');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_renting_booking`
--

DROP TABLE IF EXISTS `car_renting_booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_renting_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_status` varchar(30) NOT NULL,
  `booked_at` datetime(6) NOT NULL,
  `booking_user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car_renting_booking_booking_user_id_5af3a56e` (`booking_user_id`),
  CONSTRAINT `car_renting_booking_booking_user_id_5af3a56e_fk_accounts_` FOREIGN KEY (`booking_user_id`) REFERENCES `accounts_customuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_renting_booking`
--

LOCK TABLES `car_renting_booking` WRITE;
/*!40000 ALTER TABLE `car_renting_booking` DISABLE KEYS */;
/*!40000 ALTER TABLE `car_renting_booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_renting_bookinghistory`
--

DROP TABLE IF EXISTS `car_renting_bookinghistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_renting_bookinghistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booked_at` datetime(6) NOT NULL,
  `car_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car_renting_bookingh_user_id_85796cd3_fk_accounts_` (`user_id`),
  KEY `car_renting_bookinghistory_car_id_1a3eb87b_fk_car_renting_car_id` (`car_id`),
  CONSTRAINT `car_renting_bookingh_user_id_85796cd3_fk_accounts_` FOREIGN KEY (`user_id`) REFERENCES `accounts_customuser` (`id`),
  CONSTRAINT `car_renting_bookinghistory_car_id_1a3eb87b_fk_car_renting_car_id` FOREIGN KEY (`car_id`) REFERENCES `car_renting_car` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_renting_bookinghistory`
--

LOCK TABLES `car_renting_bookinghistory` WRITE;
/*!40000 ALTER TABLE `car_renting_bookinghistory` DISABLE KEYS */;
INSERT INTO `car_renting_bookinghistory` VALUES (17,'2019-05-19 13:55:36.529957',3,2,'accepted'),(18,'2019-05-19 13:59:05.292742',4,1,'rejected'),(19,'2019-06-04 11:40:20.075484',3,2,'accepted'),(20,'2019-06-04 13:45:43.347895',26,4,'canceled'),(21,'2019-06-04 13:45:55.099803',26,4,'canceled'),(22,'2019-06-11 04:12:06.381619',29,5,'rejected'),(23,'2019-06-11 04:13:14.338479',30,6,'accepted'),(24,'2019-06-13 02:51:03.578258',30,2,'canceled');
/*!40000 ALTER TABLE `car_renting_bookinghistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_renting_car`
--

DROP TABLE IF EXISTS `car_renting_car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_renting_car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `plate_number` varchar(8) NOT NULL,
  `availability` varchar(15) NOT NULL,
  `price` varchar(12) NOT NULL,
  `location` varchar(255) NOT NULL,
  `description` varchar(300) NOT NULL,
  `cover_image` varchar(100) NOT NULL,
  `registered_at` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_available` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car_renting_car_user_id_c63d0263_fk_accounts_customuser_id` (`user_id`),
  CONSTRAINT `car_renting_car_user_id_c63d0263_fk_accounts_customuser_id` FOREIGN KEY (`user_id`) REFERENCES `accounts_customuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_renting_car`
--

LOCK TABLES `car_renting_car` WRITE;
/*!40000 ALTER TABLE `car_renting_car` DISABLE KEYS */;
INSERT INTO `car_renting_car` VALUES (3,'Crown Athlete','DDE-765','08:00-18:00','350,000','Ubungo','I\'ve been working on shipping the latest version of Launchday. The story I\'m trying to focus on is something like \"You\'re launching soon and need to be 100% focused on your product. Don\'t lose precious days designing, coding, and testing a product site. Instead, build one in minutes.','cars/2019/05/14/harrier_7GvMxCl.jpg','2019-05-14',1,1),(4,'Toyota Harrier','DDE-765','08:00-18:00','2,000,00','Kwa aziziali, Temeke','When you raise Http404 from within a view, Django will load a special view devoted to handling 404 errors. It finds it by looking for the variable handler404 in your root URLconf (and only in your root URLconf; setting handler404 anywhere else will have no effect), which is a string in Python dotted','cars/2019/05/14/harrier_NpG5gnl.jpg','2019-05-14',2,1),(5,'Kluger','DDE-765','08:00-18:00','200,000','Kwa aziziali, Temeke','Mzizima Rally of Kigamboni 6th-7th October 2018','cars/2019/05/14/harrier_6lCkZD7.jpg','2019-05-14',2,1),(26,'ferari','das-322','08:00-19:00','3,232,323','dasdadsda','dasdsadsd','cars/2019/06/04/_E5A3126.jpg','2019-06-04',2,0),(27,'Lamborghini Aventador','DFE-323','09:00-03:00','2,000,00','Mbezi Msumi','fasdfafdfedafadfa','cars/2019/06/04/wallpaper32.jpg','2019-06-04',2,1),(28,'Audi TT','DFD-323','09:00-08:00','4,243,243','Ubungo, Terminal','fadfadfdasfdaf','cars/2019/06/04/wallpaper30.jpg','2019-06-04',4,1),(29,'v8','DDD-567','08:00-19:00','6,657,454','SDSDSDSDS','HJGJGHJGJGJHGJH','cars/2019/06/11/_E5A3130.jpg','2019-06-11',6,1),(30,'subaru','ASD-676','09:00-18:00','7,867,657','GHHJGJHGJHG','GHGFHGFGHFHG','cars/2019/06/11/_E5A3157.jpg','2019-06-11',5,1);
/*!40000 ALTER TABLE `car_renting_car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_renting_carimage`
--

DROP TABLE IF EXISTS `car_renting_carimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_renting_carimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `car_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car_renting_carimages_car_id_a16f4971_fk_car_renting_car_id` (`car_id`),
  CONSTRAINT `car_renting_carimages_car_id_a16f4971_fk_car_renting_car_id` FOREIGN KEY (`car_id`) REFERENCES `car_renting_car` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_renting_carimage`
--

LOCK TABLES `car_renting_carimage` WRITE;
/*!40000 ALTER TABLE `car_renting_carimage` DISABLE KEYS */;
INSERT INTO `car_renting_carimage` VALUES (1,'cars/images/2019/06/04/wallpapersecond.jpg',4),(10,'cars/images/2019/06/04/wallpaper32.jpg',3),(11,'cars/images/2019/06/04/wallpaper28.jpg',3),(12,'cars/images/2019/06/11/_E5A3126.jpg',29),(13,'cars/images/2019/06/11/_E5A3093.jpg',29),(14,'cars/images/2019/06/11/_E5A3126_ZujEGH4.jpg',29),(15,'cars/images/2019/06/11/_E5A3153.jpg',30);
/*!40000 ALTER TABLE `car_renting_carimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_renting_notification`
--

DROP TABLE IF EXISTS `car_renting_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_renting_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `message` varchar(300) NOT NULL,
  `target_id` int(11) NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `is_viewed` tinyint(1) NOT NULL,
  `source_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car_renting_notifica_target_id_48ad1b84_fk_accounts_` (`target_id`),
  KEY `car_renting_notifica_source_id_987c1f4a_fk_accounts_` (`source_id`),
  KEY `car_renting_notifica_booking_id_089ef207_fk_car_renti` (`booking_id`),
  CONSTRAINT `car_renting_notifica_booking_id_089ef207_fk_car_renti` FOREIGN KEY (`booking_id`) REFERENCES `car_renting_booking` (`id`),
  CONSTRAINT `car_renting_notifica_source_id_987c1f4a_fk_accounts_` FOREIGN KEY (`source_id`) REFERENCES `accounts_customuser` (`id`),
  CONSTRAINT `car_renting_notifica_target_id_48ad1b84_fk_accounts_` FOREIGN KEY (`target_id`) REFERENCES `accounts_customuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_renting_notification`
--

LOCK TABLES `car_renting_notification` WRITE;
/*!40000 ALTER TABLE `car_renting_notification` DISABLE KEYS */;
INSERT INTO `car_renting_notification` VALUES (53,'2019-05-19 13:55:18.922102','hi!! i am requesting to rent your car',1,NULL,1,2),(54,'2019-05-19 13:55:36.636250','hi!! Andrew Chauka has accepted your request for Crown Athlete',2,NULL,1,1),(55,'2019-05-19 13:58:07.813314','Andrew Chauka is requesting to rent your car',2,NULL,1,1),(56,'2019-05-19 13:59:05.388537','kibiko kibiko has rejected your request for Toyota Harrier',1,NULL,1,2),(57,'2019-06-04 11:40:07.346543','Hello!! is requesting to rent your car',1,NULL,1,2),(58,'2019-06-04 11:40:20.209478','Andrew Chauka has accepted your request for Crown Athlete',2,NULL,1,1),(59,'2019-06-04 13:45:38.868396','Hello!! is requesting to rent your car',2,NULL,1,4),(60,'2019-06-04 13:45:47.695567','Hello!! is requesting to rent your car',2,NULL,1,4),(61,'2019-06-11 04:11:52.202480','Hello!! is requesting to rent your car',6,NULL,1,5),(62,'2019-06-11 04:12:06.472976','chauka jr has rejected your request for v8',5,NULL,1,6),(63,'2019-06-11 04:12:33.178105','Hello!! is requesting to rent your car',5,NULL,1,6),(64,'2019-06-11 04:13:14.565771','Christopher shoo has accepted your request for subaru',6,NULL,1,5),(65,'2019-06-13 02:50:49.354995','Hello!! is requesting to rent your car',5,NULL,0,2);
/*!40000 ALTER TABLE `car_renting_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_renting_paymentrecord`
--

DROP TABLE IF EXISTS `car_renting_paymentrecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_renting_paymentrecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` varchar(20) NOT NULL,
  `payment_time` datetime(6) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `payee_id` int(11) NOT NULL,
  `payer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car_renting_paymentr_booking_id_1c4b3084_fk_car_renti` (`booking_id`),
  KEY `car_renting_paymentr_payee_id_61c7030e_fk_accounts_` (`payee_id`),
  KEY `car_renting_paymentr_payer_id_f3f116ab_fk_accounts_` (`payer_id`),
  CONSTRAINT `car_renting_paymentr_booking_id_1c4b3084_fk_car_renti` FOREIGN KEY (`booking_id`) REFERENCES `car_renting_booking` (`id`),
  CONSTRAINT `car_renting_paymentr_payee_id_61c7030e_fk_accounts_` FOREIGN KEY (`payee_id`) REFERENCES `accounts_customuser` (`id`),
  CONSTRAINT `car_renting_paymentr_payer_id_f3f116ab_fk_accounts_` FOREIGN KEY (`payer_id`) REFERENCES `accounts_customuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_renting_paymentrecord`
--

LOCK TABLES `car_renting_paymentrecord` WRITE;
/*!40000 ALTER TABLE `car_renting_paymentrecord` DISABLE KEYS */;
/*!40000 ALTER TABLE `car_renting_paymentrecord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_accounts_customuser_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_accounts_customuser_id` FOREIGN KEY (`user_id`) REFERENCES `accounts_customuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2019-05-14 13:48:28.749818','1','admin@admin.org',1,'[{\"added\": {}}]',2,1),(2,'2019-05-14 13:49:21.728843','1','Toyota Harrier',1,'[{\"added\": {}}]',3,1),(3,'2019-05-14 13:49:25.821094','1','Toyota Harrier',2,'[]',3,1),(4,'2019-05-14 13:49:55.723510','2','Kluger',1,'[{\"added\": {}}]',3,1),(5,'2019-05-14 13:50:24.738507','3','Crown Athlete',1,'[{\"added\": {}}]',3,1),(6,'2019-05-14 14:38:12.274081','2','christopher@gmail.com',1,'[{\"added\": {}}]',1,1),(7,'2019-05-14 14:38:30.817580','2','christopher@gmail.com',2,'[{\"changed\": {\"fields\": [\"first_name\", \"last_name\", \"is_active\"]}}]',1,1),(8,'2019-05-14 14:39:14.190930','2','christopher@gmail.com',1,'[{\"added\": {}}]',2,1),(9,'2019-05-14 14:39:26.910612','2','Kluger',2,'[{\"changed\": {\"fields\": [\"user\"]}}]',3,1),(10,'2019-05-14 14:55:20.486029','4','Toyota Harrier',1,'[{\"added\": {}}]',3,1),(11,'2019-05-14 16:03:05.129180','1','Kluger',3,'',9,1),(12,'2019-05-14 16:56:10.445055','6','Kluger',1,'[{\"added\": {}}]',9,1),(13,'2019-05-14 16:56:13.520299','1','he your kluger is being tested on a network',1,'[{\"added\": {}}]',10,1),(14,'2019-05-14 17:24:54.430545','1','he your kluger is being tested on a network',2,'[{\"changed\": {\"fields\": [\"is_viewed\"]}}]',10,1),(15,'2019-05-14 17:26:37.012987','1','he your kluger is being tested on a network',2,'[{\"changed\": {\"fields\": [\"is_viewed\"]}}]',10,1),(16,'2019-05-14 17:30:07.433755','1','he your kluger is being tested on a network',2,'[{\"changed\": {\"fields\": [\"is_viewed\"]}}]',10,1),(17,'2019-05-14 17:31:45.305871','1','he your kluger is being tested on a network',2,'[]',10,1),(18,'2019-05-14 17:32:04.765046','1','he your kluger is being tested on a network',2,'[{\"changed\": {\"fields\": [\"is_viewed\"]}}]',10,1),(19,'2019-05-14 17:34:21.503036','1','he your kluger is being tested on a network',2,'[{\"changed\": {\"fields\": [\"is_viewed\"]}}]',10,1),(20,'2019-05-14 17:34:39.102332','1','he your kluger is being tested on a network',2,'[{\"changed\": {\"fields\": [\"is_viewed\"]}}]',10,1),(21,'2019-05-14 22:51:54.757534','1','he your kluger is being tested on a network',2,'[{\"changed\": {\"fields\": [\"is_viewed\"]}}]',10,1),(22,'2019-05-14 22:55:02.547301','1','he your kluger is being tested on a network',2,'[{\"changed\": {\"fields\": [\"is_viewed\"]}}]',10,1),(23,'2019-05-14 22:55:16.509516','6','Kluger',2,'[{\"changed\": {\"fields\": [\"booking_status\"]}}]',9,1),(24,'2019-05-14 23:00:07.865587','6','Kluger',2,'[{\"changed\": {\"fields\": [\"booking_status\"]}}]',9,1),(25,'2019-05-14 23:00:18.170929','1','he your kluger is being tested on a network',2,'[{\"changed\": {\"fields\": [\"is_viewed\"]}}]',10,1),(26,'2019-05-14 23:06:42.529520','1','he your kluger is being tested on a network',2,'[{\"changed\": {\"fields\": [\"is_viewed\"]}}]',10,1),(27,'2019-05-14 23:07:18.540014','6','Kluger',2,'[{\"changed\": {\"fields\": [\"booking_status\"]}}]',9,1),(28,'2019-05-14 23:14:39.517370','7','Kluger',3,'',9,1),(29,'2019-05-14 23:58:10.040564','2','Kluger',3,'',3,1),(30,'2019-05-14 23:58:39.268439','5','Kluger',1,'[{\"added\": {}}]',3,1),(31,'2019-05-15 00:42:21.752420','23','Crown Athlete',1,'[{\"added\": {}}]',9,1),(32,'2019-05-15 00:43:17.787263','2','hi! i want to book your car',1,'[{\"added\": {}}]',10,1),(33,'2019-05-17 03:07:19.272771','2','christopher@gmail.com',2,'[{\"changed\": {\"fields\": [\"profile_image\"]}}]',2,1),(34,'2019-05-17 03:13:15.664284','3','chauka@gmail.com',2,'[{\"changed\": {\"fields\": [\"is_staff\", \"is_superuser\"]}}]',1,1),(35,'2019-05-17 03:14:57.378749','2','christopher@gmail.com',2,'[{\"changed\": {\"fields\": [\"first_name\", \"last_name\"]}}]',1,1),(36,'2019-05-17 03:15:11.510927','2','kibiko@gmail.com',2,'[{\"changed\": {\"fields\": [\"email\"]}}]',1,1),(37,'2019-05-19 12:03:50.244327','25','Toyota Harrier',3,'',9,1),(38,'2019-05-19 12:03:50.300433','24','Toyota Harrier',3,'',9,1),(39,'2019-05-19 12:03:50.345205','23','Crown Athlete',3,'',9,1),(40,'2019-05-19 12:03:50.390286','22','Kluger',3,'',9,1),(41,'2019-05-19 12:03:50.434334','18','Toyota Harrier',3,'',9,1),(42,'2019-05-19 12:23:28.486547','1','Andrew Chauka',3,'',11,1),(43,'2019-05-19 12:52:40.811078','28','Toyota Harrier',3,'',9,1),(44,'2019-05-19 12:52:40.893247','27','Toyota Harrier',3,'',9,1),(45,'2019-05-19 13:25:23.216638','43','hi!! kibiko kibiko has replied to your request',3,'',10,1),(46,'2019-05-19 13:25:23.312057','42','hi!! kibiko kibiko has replied to your request',3,'',10,1),(47,'2019-05-19 13:25:23.367620','41','hi!! kibiko kibiko has replied to your request',3,'',10,1),(48,'2019-05-19 13:25:23.423043','40','hi!! i am requesting to rent your car',3,'',10,1),(49,'2019-05-19 13:25:23.468003','39','hi!! Andrew Chauka has replied to your request',3,'',10,1),(50,'2019-05-19 13:25:23.512622','38','hi!! Andrew Chauka has replied to your request',3,'',10,1),(51,'2019-05-19 13:25:23.557199','37','hi!! i am requesting to rent your car',3,'',10,1),(52,'2019-05-19 13:28:00.367886','35','Toyota Harrier',3,'',9,1),(53,'2019-05-19 13:28:00.416158','34','Crown Athlete',3,'',9,1),(54,'2019-05-19 13:28:29.815603','13','Andrew Chauka',3,'',11,1),(55,'2019-05-19 13:28:29.878326','12','Andrew Chauka',3,'',11,1),(56,'2019-05-19 13:28:29.922719','11','Andrew Chauka',3,'',11,1),(57,'2019-05-19 13:28:29.967486','10','Andrew Chauka',3,'',11,1),(58,'2019-05-19 13:28:30.011574','9','Andrew Chauka',3,'',11,1),(59,'2019-05-19 13:28:30.056613','8','kibiko kibiko',3,'',11,1),(60,'2019-05-19 13:28:30.100825','7','kibiko kibiko',3,'',11,1),(61,'2019-05-19 13:28:30.145403','6','Andrew Chauka',3,'',11,1),(62,'2019-05-19 13:28:30.190147','5','Andrew Chauka',3,'',11,1),(63,'2019-05-19 13:28:30.234950','4','Andrew Chauka',3,'',11,1),(64,'2019-05-19 13:28:30.279600','3','Andrew Chauka',3,'',11,1),(65,'2019-05-19 13:28:30.323850','2','Andrew Chauka',3,'',11,1),(66,'2019-05-19 13:29:08.205980','46','hi!! kibiko kibiko has replied to your request',3,'',10,1),(67,'2019-05-19 13:29:08.288893','45','hi!! kibiko kibiko has replied to your request',3,'',10,1),(68,'2019-05-19 13:29:08.389086','44','hi!! i am requesting to rent your car',3,'',10,1),(69,'2019-05-19 13:35:27.939508','14','Andrew Chauka',2,'[{\"changed\": {\"fields\": [\"status\"]}}]',11,1),(70,'2019-05-19 13:49:55.061753','16','Andrew Chauka',3,'',11,1),(71,'2019-05-19 13:49:55.172684','15','Andrew Chauka',3,'',11,1),(72,'2019-05-19 13:49:55.217575','14','Andrew Chauka',3,'',11,1),(73,'2019-05-19 13:50:07.491399','52','hi!! kibiko kibiko has accepted your request for Kluger',3,'',10,1),(74,'2019-05-19 13:50:07.568434','51','hi!! i am requesting to rent your car',3,'',10,1),(75,'2019-05-19 13:50:07.612052','50','hi!! kibiko kibiko has replied to your request',3,'',10,1),(76,'2019-05-19 13:50:07.780412','49','hi!! i am requesting to rent your car',3,'',10,1),(77,'2019-05-19 13:50:07.824845','48','hi!! kibiko kibiko has replied to your request',3,'',10,1),(78,'2019-05-19 13:50:07.869604','47','hi!! i am requesting to rent your car',3,'',10,1),(79,'2019-05-19 14:01:05.457774','3','chauka@gmail.com',3,'',1,1),(80,'2019-05-19 14:01:53.116434','2','kibiko@gmail.com',2,'[{\"changed\": {\"fields\": [\"mobile_contact\"]}}]',2,1),(81,'2019-06-04 14:43:00.560579','1','Toyota Harrier',1,'[{\"added\": {}}]',13,1),(82,'2019-06-04 14:43:17.954689','2','Crown Athlete',1,'[{\"added\": {}}]',13,1),(83,'2019-06-04 14:44:31.571911','3','Crown Athlete',1,'[{\"added\": {}}]',13,1),(84,'2019-06-04 14:57:01.859106','4','Crown Athlete',1,'[{\"added\": {}}]',13,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'accounts','customuser'),(2,'accounts','userprofile'),(4,'admin','logentry'),(6,'auth','group'),(5,'auth','permission'),(9,'car_renting','booking'),(11,'car_renting','bookinghistory'),(3,'car_renting','car'),(13,'car_renting','carimage'),(10,'car_renting','notification'),(12,'car_renting','paymentrecord'),(7,'contenttypes','contenttype'),(8,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-05-14 13:46:33.326940'),(2,'contenttypes','0002_remove_content_type_name','2019-05-14 13:46:34.768759'),(3,'auth','0001_initial','2019-05-14 13:46:35.823837'),(4,'auth','0002_alter_permission_name_max_length','2019-05-14 13:46:40.516233'),(5,'auth','0003_alter_user_email_max_length','2019-05-14 13:46:40.586019'),(6,'auth','0004_alter_user_username_opts','2019-05-14 13:46:40.649789'),(7,'auth','0005_alter_user_last_login_null','2019-05-14 13:46:40.698557'),(8,'auth','0006_require_contenttypes_0002','2019-05-14 13:46:40.749678'),(9,'auth','0007_alter_validators_add_error_messages','2019-05-14 13:46:40.798453'),(10,'auth','0008_alter_user_username_max_length','2019-05-14 13:46:40.848904'),(11,'auth','0009_alter_user_last_name_max_length','2019-05-14 13:46:40.893057'),(12,'auth','0010_alter_group_name_max_length','2019-05-14 13:46:41.878126'),(13,'auth','0011_update_proxy_permissions','2019-05-14 13:46:41.937649'),(14,'accounts','0001_initial','2019-05-14 13:46:43.050674'),(15,'admin','0001_initial','2019-05-14 13:46:48.964306'),(16,'admin','0002_logentry_remove_auto_add','2019-05-14 13:46:51.136151'),(17,'admin','0003_logentry_add_action_flag_choices','2019-05-14 13:46:51.203122'),(18,'car_renting','0001_initial','2019-05-14 13:46:51.501767'),(19,'sessions','0001_initial','2019-05-14 13:46:52.857934'),(20,'car_renting','0002_car_is_available','2019-05-14 13:52:05.944719'),(21,'car_renting','0003_booking','2019-05-14 15:37:17.664752'),(22,'car_renting','0004_notification','2019-05-14 16:52:07.288210'),(23,'car_renting','0005_notification_booking','2019-05-14 16:54:59.024916'),(24,'car_renting','0006_auto_20190514_1655','2019-05-14 16:55:11.683166'),(25,'car_renting','0007_auto_20190514_1709','2019-05-14 17:09:04.969778'),(26,'car_renting','0008_auto_20190514_2314','2019-05-14 23:14:43.818227'),(27,'car_renting','0009_auto_20190514_2316','2019-05-14 23:16:34.147465'),(28,'car_renting','0010_auto_20190514_2348','2019-05-14 23:48:25.742510'),(29,'car_renting','0011_auto_20190514_2350','2019-05-14 23:50:12.275740'),(30,'car_renting','0012_auto_20190514_2353','2019-05-14 23:53:15.417146'),(31,'car_renting','0013_auto_20190515_0015','2019-05-15 00:15:35.706951'),(32,'car_renting','0014_auto_20190515_0017','2019-05-15 00:17:24.225575'),(33,'car_renting','0015_bookinghistory','2019-05-19 10:38:40.900197'),(34,'car_renting','0016_bookinghistory_status','2019-05-19 11:10:13.435840'),(35,'car_renting','0017_auto_20190519_1310','2019-05-19 13:10:47.943526'),(36,'car_renting','0018_auto_20190519_1316','2019-05-19 13:17:02.570331'),(37,'car_renting','0019_auto_20190519_1327','2019-05-19 13:27:05.220990'),(38,'car_renting','0020_auto_20190604_0908','2019-06-04 09:08:57.236277'),(39,'accounts','0002_auto_20190604_1301','2019-06-04 13:01:47.918987'),(40,'accounts','0003_auto_20190604_1405','2019-06-04 14:05:25.471587'),(41,'car_renting','0021_paymentrecord','2019-06-04 14:05:25.823048'),(42,'car_renting','0022_carimages','2019-06-04 14:40:51.769648'),(43,'car_renting','0023_auto_20190604_1510','2019-06-04 15:10:41.065558');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('ahrpwiywksino19njzy9jf4hps4b2hex','N2U4MjdlYjgxZDliNzRjM2U3ZGQxOWYyOTc3NTJlM2MxM2QzMmQwMjp7InVzZXJuYW1lIjoiY2hyaXN0b3BoZXJiZW5zb24xN0BnbWFpbC5jb20iLCJfYXV0aF91c2VyX2lkIjoiNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjI1ZWYwMzU3NDNlOTI4YmJlNDQwMTJkM2ZlNmVlODY3OTY4Y2JlNyJ9','2019-06-25 04:19:44.202321'),('cpi0ezssnq0d6oo8su25ik33j5gjyr98','ZDY1MWViNzU5ZjNiZTVhNWUxOTM5ODNkNGIwZmMzNGNjMWIzYzk4Yzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxMzE4YmNhODg3MGYwMWEzOTViMmE2MDBkYjVkOWRlNDIwMGVhMTc0In0=','2019-06-03 03:38:29.897032'),('jndaphgnoi555utozwf8jiyrloetf2tz','ZDY1MWViNzU5ZjNiZTVhNWUxOTM5ODNkNGIwZmMzNGNjMWIzYzk4Yzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxMzE4YmNhODg3MGYwMWEzOTViMmE2MDBkYjVkOWRlNDIwMGVhMTc0In0=','2019-06-27 02:54:13.773795'),('k23n4no4w7x6y2qtn9mn0a8hz9n0sjmq','N2U4MjdlYjgxZDliNzRjM2U3ZGQxOWYyOTc3NTJlM2MxM2QzMmQwMjp7InVzZXJuYW1lIjoiY2hyaXN0b3BoZXJiZW5zb24xN0BnbWFpbC5jb20iLCJfYXV0aF91c2VyX2lkIjoiNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjI1ZWYwMzU3NDNlOTI4YmJlNDQwMTJkM2ZlNmVlODY3OTY4Y2JlNyJ9','2019-06-25 04:28:58.631043'),('lu8v3bm3ytro3g16fyf7mm4o9dymqgw9','Y2Y4ZjgxZTVhNTVhNDhjNGIxYzkzOWRiMDBmMWQwYTBjYzM0NGNjNjp7InVzZXJuYW1lIjoiZWxpb3RAZ21haWwuY29tIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjAxODEwNjJmYTZjNzdjOGIwZDhlMjdlNWNlZDYyYzIyODliOWJhMDkifQ==','2019-06-18 15:33:18.781935'),('t8bdfgfv54q33eo31v6gubrdz1wudqkp','NmJiNjBiM2ViNmRiYWM0MGNkZTIzYmVhYWY3YjdiMDNkOGQyOTU5YTp7InVzZXJuYW1lIjoia2liaWtvQGdtYWlsLmNvbSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZTVhMjIyNGM5YTkyYzJhYjEwM2NjMDFkNGU5MTU2NWQ1ZGI0ZGQ0In0=','2019-06-18 08:56:32.494273'),('x8ruv00gap7ersxn2a3klhei00xvz2hm','ZTU4MjE0Y2E1MjNlZjI3ODU0MjUzNjQ4MTFiY2Y4MTY5ZDViNjMwNDp7InVzZXJuYW1lIjoiY2hhdWthanJAZ21haWwuY29tIiwiX2F1dGhfdXNlcl9pZCI6IjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjI2YmVjNmQ1YzA4MDk5ZTlkNmY1ZDM4YmIxOGZhM2RlMTA0YWNkMTMifQ==','2019-06-25 04:07:10.150403');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-13  6:10:03
