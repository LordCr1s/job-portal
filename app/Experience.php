<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    
    protected $fillable = [
        'place', 'job_title', 'job_duration', 'user_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
