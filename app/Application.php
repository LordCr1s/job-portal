<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    
    protected $fillable = [
        'aplication_later', 'vacancy_id', 'user_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function vacancy(){
        return $this->belongsTo('App\Vacancy');
    }
}
