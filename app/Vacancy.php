<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{

    protected $fillable = [
        'title', 'body', 'position', 'application_deadline', 'user_id', 'function_id', 'industry_id', 'location_id', 'working_hour_id', 
    ];

    public function workinghour(){
        return $this->belongsTo('App\WorkingHour');
    }

    public function location(){
        return $this->belongsTo('App\Location');
    }

    public function industry(){
        return $this->belongsTo('App\Industry');
    }

    public function jobfunction(){
        return $this->belongsTo('App\JobFunction');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function applications(){
        return $this->hasMany('App\Application');
    }
}
