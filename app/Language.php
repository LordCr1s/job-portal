<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{

    protected $fillable = [
        'language', 'speaking', 'reading', 'writting', 'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
