<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobFunction extends Model
{
    public function vacancy(){
        return $this->hasOne('App\Vacancy');
    }
}
