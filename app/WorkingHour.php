<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkingHour extends Model
{
    public function vacancy(){
        return $this->hasOne('App\Vacancy');
    }
}
