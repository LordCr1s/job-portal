<?php

namespace App;


use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\VerifyApiEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail , JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function academics(){
        return $this->hasMany('App\Academic');
    }

    public function languages(){
        return $this->hasMany('App\Language');
    }
    
    public function contacts(){
        return $this->hasOne('App\Contact');
    }

    public function applications(){
        return $this->hasMany('App\Application');
    }

    public function vacancies(){
        return $this->hasMany('App\Vacancy');
    }

    public function studentprofile(){
        return $this->hasOne('App\StudentProfile');
    }

    public function companyprofile(){
        return $this->hasOne('App\CompanyProfile');
    }

    public function experiences()
    {
        return $this->hasMany('App\Experience');
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'This action is unauthorized.');
    }
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role){
                if ($this->hasRole($role)){
                    return true;
                }
            }
        } 
        else {
            if ($this->hasRole($roles)){
            return true;
            }
        }
        return false;
    }
    public function hasRole($role){
        if ($this->roles()->where('name', $role)->first()){
            return true;
        }
        return false;
    }

     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Send email verification notification via api
     *
     * @return notification
     */
    public function sendApiEmailVerificationNotification()
    {
        $this->notify(new VerifyApiEmail); 
    }
}
