<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    
    protected $fillable = [
        'present_address', 'permenent_address', 'country', 'city','province','telephone','mobile','user_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
