<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{

    protected $fillable = [
        'level', 'programe', 'institution', 'year','user_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function accademics(){
        return $this->belongsTo('App\Academic');
    }
}
