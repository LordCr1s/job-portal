<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Academic extends Model
{

    protected $fillable = [
        'level', 'programe', 'institution', 'year','user_id', 'attachment_id'
    ];


    public function user(){
        return $this->belongsTo('App\User');
    }

    public function attachment(){
        return $this->belongsTo('App\Attachment');
    }
}
