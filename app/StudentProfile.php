<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentProfile extends Model
{
    protected $fillable = [
        'first_name',
        'middle_name' ,
        'last_name' ,
        'date_of_birth',
        'gender',
        'marital_status',
        'nationality' ,
        'mobile_phone',
        'place_of_birth',
        'disability' ,
        'image' ,
        'user_id',
    ];
    


    public function user(){
        return $this->BelongsTo('App\User');
    }
}
