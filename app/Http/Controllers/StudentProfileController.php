<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Storage;
use App\Academic;
use App\StudentProfile;
use App\Contact;
use App\Language;
use App\Experience;
use App\Attachment;
use App\Role;
use App\User; 
use Validator;

class StudentProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('updateImage');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
            $role = Role::where('name', 'student')->first();
           // $profile = StudentProfile::whereIn('user_id', $role->users->pluck('id'))->get();
            $students = $role->users->each(function ($item) use (&$role) {
                    
                    $item->studentprofile;
                    $item->user;
                    $item->contacts;
                    $item->academics;
                    $item->languages;
                });
            
           return response()->json(['students' => $students], 200);
        
    }


    public function show($id){
        $user = auth('api')->user();
        $student = User::where('id', $id)->with('academics','contacts', 'languages','studentprofile')->get();
        return response()->json(['student' =>$student, 'status'=>200 ], 200);
        
    }

    /**
     * store new student profile
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $user = auth('api')->user();

        if($request->action == 'academics'){
            $valid = Validator::make($request->all(), [
                'level' => 'required|string', 
                'programe' => 'required|string',
                'institution' => 'required|string',
                'year' => 'required|string',
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);
    
            }else{
                
                $academic = Academic::create([
                    'level' => $request['level'],
                    'programe' => $request['programe'],
                    'year' => $request['year'],
                    'institution' => $request['institution'],
                    'user_id' => $user->id
                ]);

                return response()->json(['success' => 'academic info stored successful', 'status'=>200], 200);  
            }
        }elseif ($request->action == 'personal_info') {
            $valid = Validator::make($request->all(), [
                'first_name' => 'required|string',
                'middle_name' => 'required|string',
                'last_name' => 'required|string',
                'date_of_birth' => 'required|string',
                'gender' => 'required|string',
                'marital_status' => 'required|string',
                'nationality' => 'required|string',
                'mobile_phone' => 'required|string',
                'place_of_birth' => 'required|string',
                'disability' => 'required|string',
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=> 401], 401);
    
            }else{
                
                if($request->hasFile('image')){
                    $filename_ext = $request->file('image')->getClientOriginalExtension();
                    $filetostore = 'OREMS'.'_'. time().'.'.$filename_ext;
                    $path = $request->file('image')->storeAs('public/student/image', $filetostore);
                    $profile = StudentProfile::create([
                    'first_name' => $request['first_name'],
                    'middle_name' => $request['middle_name'],
                    'last_name' => $request['last_name'],
                    'date_of_birth' => $request['date_of_birth'],
                    'gender' => $request['gender'],
                    'marital_status' => $request['marital_status'],
                    'nationality' => $request['nationality'],
                    'mobile_phone' => $request['mobile_phone'],
                    'place_of_birth' => $request['place_of_birth'],
                    'disability' => $request['disability'],
                    'image' => $path,
                    'user_id' => $user->id,
                    
                    ]);

                    return response()->json(['success' => 'personal info stored successful', 'status'=>200], 200);  

                }else{
                        $profile = StudentProfile::create([
                        'first_name' => $request['first_name'],
                        'middle_name' => $request['middle_name'],
                        'last_name' => $request['last_name'],
                        'date_of_birth' => $request['date_of_birth'],
                        'gender' => $request['gender'],
                        'marital_status' => $request['marital_status'],
                        'nationality' => $request['nationality'],
                        'mobile_phone' => $request['mobile_phone'],
                        'place_of_birth' => $request['place_of_birth'],
                        'disability' => $request['disability'],
                        'user_id' => $user->id,
                        ]); 
                }
                return response()->json(['success' => 'personal info stored successful', 'status'=>200], 200);  
            }
        }elseif ($request->action == 'contact') {

                $valid = Validator::make($request->all(), [
                'present_address' => 'required|string',
                'permenent_address' => 'required|string',
                'country' => 'required|string',
                'city' => 'required|string',
                'province' => 'required|string',
                'telephone' => 'required|string',
                'mobile' => 'required|string',
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);
    
            }else{
    
                $contact = Contact::create([
                    'present_address' => $request['present_address'],
                    'permenent_address' => $request['permenent_address'],
                    'country' => $request['country'],
                    'city' => $request['city'],
                    'province' => $request['province'],
                    'telephone' => $request['telephone'],
                    'mobile' => $request['mobile'],
                    'user_id' => $user->id
                ]);

                return response()->json(['success' => 'academic info stored successful', 'status'=>200], 200);  
            }
             
        }elseif ($request->action == 'language') {
            $valid = Validator::make($request->all(), [
                'language' => 'required|string',
                'speaking' => 'required|string',
                'writting' => 'required|string',
                'reading' => 'required|string',
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);
    
            }else{
    
                $academic = Language::create([
                    'language' => $request['language'],
                    'speaking' => $request['speaking'],
                    'writting' => $request['writting'],
                    'reading' => $request['reading'],
                    'user_id' => $user->id
                ]);

                return response()->json(['success' => 'language info stored successful', 'status'=>200], 200);  
            }
        }elseif ($request->action == 'experience') {
            //dd($request->all());
            $valid = Validator::make($request->all(), [
                'place' => 'required|string',
                'title' => 'required|string',
                'duration' => 'required|string',
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);
    
            }else{
    
                $academic = Experience::create([
                    'place' => $request['place'],
                    'job_title' => $request['title'],
                    'job_duration' => $request['duration'],
                    'user_id' => $user->id
                ]);

                return response()->json(['success' => 'language info stored successful', 'status'=>200], 200);  
            }
        }else {
            $valid = Validator::make($request->all(), [
                'title' => 'required|string',
                'file ' => 'required|mimes:jpeg,bmp,png,pdf',
                
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors()], 401);
    
            }else{
                $filename_ext = $request->file('file')->getClientOriginalExtension();
                $filetostore = 'OREMS'.'_'. time().'.'.$filename_ext;
                $path = $request->file('file')->storeAs('public/student/attachment', $filetostore);
                $academic = Attachment::create([
                    'name' => $request['title'],
                    'file' => $request['file'],
                    'user_id' => $user->id
                ]);

                return response()->json(['success' => 'attachment stored successful', 'status'=>200], 200);  
            }
        }

        
    }

    public function update(Request $request){

        $user = auth('api')->user();
        
        if($request->action == 'academics'){
            $valid = Validator::make($request->all(), [
                'level' => 'required|string',
                'programe' => 'required|string',
                'institution' => 'required|string',
                'year' => 'required|string',
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=> 401], 401);
    
            }else{
    
                $academic = Academic::where('user_id', $user->id)->update([
                    'level' => $request['level'],
                    'programe' => $request['programe'],
                    'year' => $request['year'],
                    'institution' => $request['institution'],
                ]);

                return response()->json(['success' => 'academic info stored successful', 'status'=> 200], 200);  
            }
        }elseif ($request->action == 'personal_info') {
            $valid = Validator::make($request->all(), [
                'first_name' => 'required|string',
                'middle_name' => 'required|string',
                'last_name' => 'required|string',
                'date_of_birth' => 'required|string',
                'gender' => 'required|string',
                'marital_status' => 'required|string',
                'nationality' => 'required|string',
                'mobile_phone' => 'required|string',
                'place_of_birth' => 'required|string',
                'disability' => 'required|string',
                
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=> 401], 401);
    
            }else{
                
                if($request->hasFile('image')){
                    $filename_ext = $request->file('image')->getClientOriginalExtension();
                    $filetostore = 'OREMS'.'_'. time().'.'.$filename_ext;
                    $path = $request->file('image')->storeAs('public/student/image', $filetostore);
                    $profile = StudentProfile::where('user_id', $user->id)->update([
                    'first_name' => $request['first_name'],
                    'middle_name' => $request['middle_name'],
                    'last_name' => $request['last_name'],
                    'date_of_birth' => $request['date_of_birth'],
                    'gender' => $request['gender'],
                    'marital_status' => $request['marital_status'],
                    'nationality' => $request['nationality'],
                    'mobile_phone' => $request['mobile_phone'],
                    'place_of_birth' => $request['place_of_birth'],
                    'disability' => $request['disability'],
                    'image' => $path,
                    
                    ]);

                    return response()->json(['success' => 'personal info stored successful', 'status'=> 200], 200);  

                }else{
                        $profile = StudentProfile::updateOrCreate(
                        ['user_id'=> $user->id],
                        [
                        'first_name' => $request['first_name'],
                        'middle_name' => $request['middle_name'],
                        'last_name' => $request['last_name'],
                        'date_of_birth' => $request['date_of_birth'],
                        'gender' => $request['gender'],
                        'marital_status' => $request['marital_status'],
                        'nationality' => $request['nationality'],
                        'mobile_phone' => $request['mobile_phone'],
                        'place_of_birth' => $request['place_of_birth'],
                        'disability' => $request['disability'],
                        ]); 
                }
                return response()->json(['success' => 'personal info stored successful', 'status'=>200], 200);  
            }
        }elseif ($request->action == 'contact') {

                $valid = Validator::make($request->all(), [
                'present_address' => 'required|string',
                'permenent_address' => 'required|string',
                'country' => 'required|string',
                'city' => 'required|string',
                'province' => 'required|string',
                'telephone' => 'required|string',
                'mobile' => 'required|string',
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=> 401], 401);
    
            }else{
    
                $contact = Contact::updateOrCreate(
                    ['user_id' => $user->id],
                    [
                    'present_address' => $request['present_address'],
                    'permenent_address' => $request['permenent_address'],
                    'country' => $request['country'],
                    'city' => $request['city'],
                    'province' => $request['province'],
                    'telephone' => $request['telephone'],
                    'mobile' => $request['mobile'],
                ]);

                return response()->json(['success' => 'academic info stored successful', 'status'=> 200], 200);  
            }
             
        }elseif ($request->action == 'language') {
            $valid = Validator::make($request->all(), [
                'language' => 'required|string',
                'speaking' => 'required|string',
                'writting' => 'required|string',
                'reading' => 'required|string',
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);
    
            }else{
    
                $academic = Language::where('user_id', $user->id)->where('id', $request['id'])->update([
                    'language' => $request['language'],
                    'speaking' => $request['speaking'],
                    'writting' => $request['writting'],
                    'reading' => $request['reading'],
                ]);

                return response()->json(['success' => 'language info stored successful', 'status'=>200], 200);  
            }
        }elseif ($request->action == 'experience') {
            $valid = Validator::make($request->all(), [
                'place' => 'required|string',
                'title' => 'required|string',
                'duration' => 'required|string',
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=> 401], 401);
    
            }else{
    
                $academic = Experience::where('user_id', $user->id)->update([
                    'place' => $request['place'],
                    'job_title' => $request['title'],
                    'job_duration' => $request['duration'],
                ]);

                return response()->json(['success' => 'language info stored successful', 'status'=> 200], 200);  
            }
        }else {
            $valid = Validator::make($request->all(), [
                'title' => 'required|string',
                'file ' => 'required|mimes:jpeg,bmp,png,pdf',
                
            ]);
            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);
    
            }else{
                $filename_ext = $request->file('file')->getClientOriginalExtension();
                $filetostore = 'OREMS'.'_'. time().'.'.$filename_ext;
                $path = $request->file('file')->storeAs('public/student/attachment', $filetostore);
                $academic = Attachment::where('user_id', $user->id)->update([
                    'name' => $request['title'],
                    'file' => $request['file'],
                ]);

                return response()->json(['success' => 'attachment stored successful', 'status'=>200], 200);  
            }
        }


    }

    public function updateImage(Request $request) 
    {
       // dd(auth('api')->user());
        $user_id =  auth('api')->user()->id;
        if($request->all()){
            
            $filename_ext = $request->file('image')->getClientOriginalExtension();
            $filetostore = 'OREMS-student-img'.'_'. time().'.'.$filename_ext;
            $path = $request->file('image')->storeAs('public/students/images', $filetostore);
            $profile = StudentProfile::where('user_id', $user_id)->update([
            'image' => 'storage/students/images/'. $filetostore,
            ]);
            return response()->json(['success' => 'profile picture updated successful', 'status'=>200], 200);  
            }else{
            return response()->json(['success' => 'profile picture not updated successful', 'status'=>402], 402);    
            }

    }

    public function uploadApplicationLater(Request $request) 
    {
       dd($request['vacancy_id']);
       // $user_id =  1;

        if($request->hasFile('later')){
            
            $filename_ext = $request->file('later')->getClientOriginalExtension();
            $filetostore = 'OREMS-student-later'.'_'. time().'.'.$filename_ext;
            $path = $request->file('later')->storeAs('public/students/laters', $filetostore);
            $profile = StudentProfile::where('user_id', $user_id)->update([
            'later' => 'storage/students/laters'. $filetostore,
            ]);
            return response()->json(['success' => 'later uploaded successful', 'status'=>200], 200);  
            }else{
            return response()->json(['success' => 'later not uploaded successful', 'status'=>402], 402);    
            }

        }

   
}
