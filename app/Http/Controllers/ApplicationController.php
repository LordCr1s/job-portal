<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use App\Vacancy;
use App\StudentProfile;
use App\Notifications\NewApplication;
use App\Notifications\AcceptedApplication;
use App\Notifications\RejectedApplication;
use App\Notifications\CanceledApplication;
use App\Notifications\CallForMeeting;
use Illuminate\Support\Facades\Storage;
use App\User;
use Validator;
use PDF;

class ApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index_for_company(){

    }


    public function store(Request $request){

            $user = auth('api')->user();
            $valid = Validator::make($request->all(), [
                'later' => 'required',
                'vacancy_id' => 'required|string',

            ]);

            if($valid->fails()){
                return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);

            }else{
                if($request->hasFile('later')){

                $filename_ext = $request->file('later')->getClientOriginalExtension();
                $filetostore = 'OREMS-student-later'.'_'. time().'.'.$filename_ext;
                $path = $request->file('later')->storeAs('public/students/laters', $filetostore);
                $academic = Application::create([
                    'aplication_later' => 'storage/students/laters'. $filetostore,
                    'vacancy_id' => $request['vacancy_id'],
                    'user_id' => $user->id,
                ]);
                $company_id = Vacancy::where('id', $request['vacancy_id'])->first();
                //dd($company_id->id);
                $user = User::where('id', $company_id->user_id)->first();
               // dd($user);
                $user->notify(new NewApplication($company_id));
                return response()->json(['success' => 'later uploaded successful', 'status'=>200], 200);
                }else{
                return response()->json(['error' => 'later not uploaded successful', 'status'=>402], 402);
                }

            }

    }

    public function cancelVacancy(Request $request){

        $valid = Validator::make($request->all(), [
            'application_id' => 'required|integer',
            'status'=> 'required|string'
        ]);

        if($valid->fails()){
            return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);

        }else{

            $app = Application::where('id', $request['application_id'])->update([
                'status' => $request['status'],
            ]);
            $application = Application::where('id', $request['application_id'])->first();
            $vacancy = Vacancy::where('id', $application->vacancy_id)->first();
            $user = User::where('id',$vacancy->user_id)->first();
            $user->notify(new CanceledApplication($vacancy));

            return response()->json(['success' => 'application info updated successful', 'status'=>200], 200);
        }

    }

    public function acceptVacancy(Request $request){

        $valid = Validator::make($request->all(), [
            'application_id' => 'required|integer',
            'status'=> 'required|string'
        ]);

        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{

            $application = Application::where('id', $request['application_id'])->update([
                'status' => $request['status'],
            ]);
            $application = Application::where('id', $request['application_id'])->first();
            $vacancy = Vacancy::where('id', $application->vacancy_id)->first();
            $user = User::where('id',$application->user_id)->first();
            $user->notify(new AcceptedApplication($vacancy));

            return response()->json(['success' => 'application info updated successful', 'status'=>200], 200);
        }

    }


    public function rejectVacancy(Request $request){

        $valid = Validator::make($request->all(), [
            'application_id' => 'required|integer',
            'status'=> 'required|string'
        ]);

        if($valid->fails()){
            return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);

        }else{

            $application = Application::where('id', $request['application_id'])->update([
                'status' => $request['status'],
            ]);
            $application = Application::where('id', $request['application_id'])->first();
            $vacancy = Vacancy::where('id', $application->vacancy_id)->first();
            $user = User::where('id',$application->user_id)->first();
            $user->notify(new RejectedApplication($vacancy));

            return response()->json(['success' => 'application info updated successful', 'status'=>200], 200);
        }

    }

    public function callForMeeting(Request $request){

        $valid = Validator::make($request->all(), [
            'studentprofile_id' => 'required|integer',
        ]);

        if($valid->fails()){
            return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);

        }else{


            $companyprofile = CompanyProfile::where('user_id', auth('api')->user()->id)->first();
            $profile = StudentProfile::where('id', $request['studentprofile_id'])->first();
            $user = User::where('id',$profile->user_id)->first();
            $user->notify(new CallForMeeting($companyprofile ));

            return response()->json(['success' => 'application info updated successful', 'status'=>200], 200);
        }

    }

    public function downloadCv($application_id)
    {
        $valid = Validator::make(['application_id'=>$application_id], [
            'application_id' => 'required|integer',
        ]);

        if($valid->fails()){
            return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);

        }else{
            $application = Application::where('id', $application_id)->first();
            $user = User::where('id', $application->user_id)->with(['studentprofile', 'applications' => function ($aplication){
                $aplication->with(['vacancy'=> function ($vacancy){$vacancy->with(['user'=> function ($user){$user->with('companyprofile')->first();}])->first();}])->get();
                },'contacts', 'languages', 'academics', 'experiences'])->first();
                $pdf = PDF::loadHTML($this->cv($application_id));
                $pdf->setPaper('letter', 'portrait');
                // $pdf->render();
                $file1 = $pdf->output();
                $file = 'storage/students/cvs/'. $user->email.'JPGS.pdf';
                Storage::put('public/students/cvs/'. $user->email.'JPGS.pdf', $file1);
                $pdf->save($file);
            return response()->json(['file'=> $file, 'status'=>200],200);
        }


    }

    public function downloadAppLetter($application_id)
    {
        $valid = Validator::make(['application_id'=>$application_id], [
            'application_id' => 'required|integer',
        ]);

        if($valid->fails()){
            return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);

        }else{
            $application = Application::where('id', $application_id)->first();
           // dd(public_path(). $application->aplication_later);
            $file = public_path(). $application->aplication_later;
            $file1 = $application->aplication_later;
            // return $file;
            return response()->json(['file'=> $file1, 'status'=>200],200);
        }


    }


    public function delete($id){
        
    }
     
    public function cv($id){
        $application = Application::where('id', $id)->first();
            $user = User::where('id', $application->user_id)->with(['studentprofile', 'applications' => function ($aplication){
                $aplication->with(['vacancy'=> function ($vacancy){$vacancy->with(['user'=> function ($user){$user->with('companyprofile')->first();}])->first();}])->get();
                },'contacts', 'languages', 'academics', 'experiences'])->first();
                return view('cv')->with(['user'=>$user]);
    }
}
