<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Role;
use App\StudentProfile;
use App\CompanyProfile;
use App\Academic;
use App\Contact;
use App\Language;
use App\Experience;
use App\Attachment;

class AuthController extends Controller
{
      /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * register a user to a system through activation code
     * @param \Illuminate\Http\Request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request){

       $valid = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role_id' => 'required|string|min:0',
        ]);
        
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);

        }else{

            $user = User::create([
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'email_verified_at' => '2019-06-09 10:58:11',
            ]);
            $user->roles()->attach(Role::where('id', $request['role_id'])->first());
            $role = Role::where('id', $request['role_id'])->first();
            if($role->name === 'student'){
                $profile = StudentProfile::create([
                    'user_id' => $user->id,
                    'first_name' => '',
                    'middle_name' => '',
                    'last_name' => '',
                    'date_of_birth' => '',
                    'gender' => '',
                    'marital_status' => '',
                    'nationality' => '',
                    'mobile_phone' => '',
                    'place_of_birth' => '',
                    'disability' => '',
                    'image' => '',
                ]);
                $cantact = Contact::create([
                    'user_id' => $user->id,
                ]);
                $academic = Academic::create([
                    'user_id' => $user->id,
                ]);
                $language = Language::create([
                    'user_id' => $user->id,
                ]);
                $experience = Experience::create([
                    'user_id'=> $user->id
                ]);
                $attachment = Attachment::create([
                    'user_id'=> $user->id
                ]);
            }else{
                $profile = CompanyProfile::create([
                    'user_id' => $user->id,
                    'company_name' => '',
                    'website' => '',
                    'number_of_employees' => '',
                    'industry' => '',
                    'about' => '',
                    'contact_person' => '',
                    'email_for_notification' => '',
                    'phone_number' => '',
                    'address' => '', 
                    'country' => '',
                ]);
            }
            $user->sendApiEmailVerificationNotification();
            return response()->json(['success' => 'successful registered', 'status'=>200], 200);
            
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        
        $credentials = request(['email', 'password']);
        $user = User::where('email', $request->email)->first();
       
        if($user != null ){
            $date = $user->email_verified_at;
            if ($date != null && ! $token = auth()->guard('api')->attempt($credentials)) {
                return response()->json(['error' => 'Unauthorized', 'status'=>401], 401);
            }

            return $this->respondWithToken($token);

        }elseif ($user == null) {
            return response()->json(['error'=>'Your not registered', 'status'=> 412], 412);
        }else
        {

            return response()->json(['error'=>'Email not Verified', 'status'=> 412], 412);
        }

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $id = auth('api')->user()->id;
        $rolegetter = User::where('id',$id)->with(['roles'])->first();
        //dd($rolegetter->roles[0]);
        if( $rolegetter->roles[0]->name === 'company')
        {
            $user = User::where('id',$id)->with(['companyprofile','vacancies'=> function($vacancy){
                $vacancy->with(['jobfunction','workinghour', 'industry', 'location', 'applications'=> function ($application){$application->with(['user'=> function ($user){$user->with('studentprofile')->first();}])->get();}]);}])->first();
            $role = $rolegetter->roles[0];
            //dd($usercompany);
            return response()->json(['role'=> $role, 'user'=> $user , 'status'=>200]);

        }else{
            $user = User::where('id',$id)->with(['studentprofile', 'applications' => function ($aplication){ 
                    
                    $aplication->with(['vacancy'=> function ($vacancy){$vacancy->with(['user'=> function ($user){$user->with('companyprofile')->first();}])->get();}])->get();
                    },'contacts', 'languages', 'academics', 'experiences'])->first();
            $role = $rolegetter->roles[0];
            return response()->json(['role'=> $role, 'user'=> $user, 'status'=>200 ]);
        }
         
       
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'user' => $this->guard('api')->user(),
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'status' => 200
        ], 200);
    }

    public function guard(){
        return \Auth::Guard('api');
    }

}
