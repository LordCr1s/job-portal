<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\JobFunction;
use App\Industry;
use App\Location;
use App\Vacancy;
use App\StudentProfile;
use App\CompanyProfile;
use App\WorkingHour;
use Validator;

class VacancyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    public function index_for_company(){

        $user = auth('api')->user();
        $company_all_vacancies = Vacancy::where('user_id', $user->id)->get();

        return response()->json(['company_vacancies' => $company_all_vacancies, 'status'=> 200], 200);

    }

    public function index_for_student(){
        $user = auth('api')->user();
        $student_all_vacancies = Vacancy::all()->each(function ($item) {
            $profile = $item->user;
            $profile->companyprofile;
            $item->location;
            $item->industry;
            $item->jobfunction;
            $item->workinghour;
        });

        return response()->json(['student_vacancies' => $student_all_vacancies, 'status'=> 200], 200);

    }

    public function store(Request $request){

        $user = auth('api')->user();
        $valid = Validator::make($request->all(), [
            'title' => 'required|string',
            'body' => 'required|string',
            'position' => 'required|string',
            'application_deadline' => 'required|string',
            'function_id' => 'required|string',
            'industry_id' => 'required|string',
            'location_id' => 'required|string',
            'working_hour_id' => 'required|string',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);

        }else{

            $academic = Vacancy::create([
                'title' => $request['title'],
                'body' => $request['body'],
                'position' => $request['position'],
                'application_deadline' => $request['application_deadline'], 
                'function_id' => $request['function_id'],
                'industry_id' => $request['industry_id'],
                'location_id' => $request['location_id'],
                'working_hour_id' => $request['working_hour_id'],
                'user_id' => $user->id
            ]);

            return response()->json(['success' => 'vacancy info stored successful', 'status'=>200], 200);  
        }


    }


    public function update($id, Request $request){

        $user = auth('api')->user();
        $valid = Validator::make($request->all(), [
            'title' => 'required|string',
            'body' => 'required|string',
            'position' => 'required|string',
            'application_deadline' => 'required|string',
            'function_id' => 'required|string',
            'industry_id' => 'required|string',
            'location_id' => 'required|string',
            'working_hour_id' => 'required|string',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors(), 'status'=>401], 401);

        }else{

            $academic = Vacancy::where('id', $id)->update([
                'title' => $request['title'],
                'body' => $request['body'],
                'position' => $request['position'],
                'application_deadline' => $request['application_deadline'], 
                'function_id' => $request['function_id'],
                'industry_id' => $request['industry_id'],
                'location_id' => $request['location_id'],
                'working_hour_id' => $request['working_hour_id'],
            ]);

            return response()->json(['success' => 'vacancy info stored successful', 'status'=>200], 200);  
        }


    }

    public function formHelper()
    {
        $loactions = Location::all();
        $jobfunctions = JobFunction::all();
        $workinghours = WorkingHour::all();
        $industries = Industry::all();

        return response()->json(['locations'=>$loactions, 'jobfunctions'=> $jobfunctions, 'workinghours'=>$workinghours, 'industries'=>$industries, 'status'=>200], 200);
    }
   

}
