<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyProfile;
use App\User;
use App\Role;
use Validator;

class CompanyProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    
    public function index(){

        $user = auth('api')->user();
        $role = Role::where('name', 'company')->first();
        $company_profile = CompanyProfile::whereIn('user_id', $role->users->pluck('id'))->get()->each(function ($query) {
            $user = $query->user;
            $vacancies = $user->vacancies;
            $vacancies->each(function ($vacancy){
                $vacancy->location;
                $vacancy->industry;
                $vacancy->jobfunction;
                $vacancy->user->companyprofile;
            });
           

        });

        return response()->json(['companyprofile'=> $company_profile, 'status'=> 200], 200);
    }

    public function store(Request $request){

        $user = auth('api')->user();
        
        $valid = Validator::make($request->all(), [
            'company_name' => 'required|string',
            'website' => 'required|string',
            'number_of_employees' => 'required|string',
            'industry' => 'required|string',
            'about' => 'required|string',
            'contact_person' => 'required|string',
            'email_for_notification' => 'required|string',
            'phone_number' => 'required|string',
            'address' => 'required|string', 
            'country' => 'required|string',

        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors(), 'status'=> 401], 401);

        }else{

            $academic = CompanyProfile::updateOrCreate(
                ['user_id'=> $user->id],
                [
                'company_name' => $request['company_name'],
                'website' => $request['website'],
                'number_of_employees' => $request['number_of_employees'],
                'industry' => $request['industry'],
                'about' => $request['about'],
                'contact_person' => $request['contact_person'],
                'email_for_notification' => $request['email_for_notification'],
                'phone_number' => $request['phone_number'],
                'address' => $request['address'],
                'country' => $request['country'],
                'user_id' => $user->id
            ]);

            return response()->json(['success' => 'company profile info stored successful', 'status'=> 200], 200);  
        }
    }

    public function update($id, Request $request){

        $user = auth('api')->user();
        //dd($request->all());
        $valid = Validator::make($request->all(), [
            'company_name' => 'required|string',
            'website' => 'required|string',
            'number_of_employees' => 'required|string',
            'industry' => 'required|string',
            'about' => 'required|string',
            'contact_person' => 'required|string',
            'email_for_notification' => 'required|string',
            'phone_number' => 'required|string',
            'address' => 'required|string',
            'country' => 'required|string',

        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors(), 'status'=> 401], 401);

        }else{

            $academic = CompanyProfile::where('user_id', $id)->update([
                'company_name' => $request['company_name'],
                'website' => $request['website'],
                'number_of_employees' => $request['number_of_employees'],
                'industry' => $request['industry'],
                'about' => $request['about'],
                'contact_person' => $request['contact_person'],
                'email_for_notification' => $request['email_for_notification'],
                'phone_number' => $request['phone_number'],
                'address' => $request['address'],
                'country' => $request['country'],

            ]);

            return response()->json(['success' => 'company profile info edited successful', 'status'=> 200], 200);  
        }
    }
 
    public function show($id){

        $profile = CompanyProfile::where('user_id', $id)->first();
        return response()->json(['company_profile'=> $profile, 'status'=>200], 200);

    }
}
