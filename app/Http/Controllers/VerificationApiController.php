<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;
class VerificationApiController extends Controller
{

    use VerifiesEmails;

    /**
    * Mark the authenticated user’s email address as verified.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function verify($id) {
        $user = User::findOrFail($id);
        $date = date("Y-m-d g:i:s");
        $user->email_verified_at = $date; 
        $user->save();
        return response()->json('Email verified!', 200);
    }

    /**
    * Resend the email verification notification.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function resend($id)
    {
        $user = User::findOrFail($id);
        if ($user->hasVerifiedEmail()) {
            return response()->json('User already have verified email!', 422);
        }else {
            $user->sendApiEmailVerificationNotification();
            return response()->json('The notification has been resubmitted');
        }
        
    }
}