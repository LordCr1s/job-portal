<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model
{

    
    protected $fillable = [
        'company_name', 'website', 'number_of_employees', 'industry','about','contact_person',
        'email_for_notification', 'phone_number', 'address', 'country', 'user_id',   
    ]; 
    
    public function user(){
        return $this->belongsTo('App\User');
    }
}
