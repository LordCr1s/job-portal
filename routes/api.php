<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth' 
], function ($router) { 
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('register', 'AuthController@register');
});

Route::group([
    'middleware' => 'api'
], function($router) {

    //auth routes for email verification and password reset
    Route::get('email/verify/{id}', 'VerificationApiController@verify')->name('verificationapi.verify');
    Route::get('email/resend/{id}', 'VerificationApiController@resend')->name('verificationapi.resend');
    Route::post('forgetpassword', 'ApiPasswordReset@sendResetLinkEmail');
    Route::post('resetpassword', 'ApiPasswordReset@reset')->name('apipassword.reset');

});


Route::group([
    'middleware' => 'api',
    'prefix' => 'student'
], function ($router) {
    Route::get('home', 'StudentProfileController@index');
    Route::post('store', 'StudentProfileController@store'); 
    Route::get('show/{id}', 'StudentProfileController@show');
    Route::post('update', 'StudentProfileController@update');
    Route::delete('delete/{id}', 'StudentProfileController@delete'); 
    
});

Route::POST('student/update_image', 'StudentProfileController@updateImage');
Route::POST('student/upload_later', 'StudentProfileController@uploadApplicationLater');

Route::group([
    'middleware' => 'api',
    'prefix' => 'company'
], function ($router) {
    Route::get('home', 'CompanyProfileController@index');
    Route::post('store', 'CompanyProfileController@store');
    Route::get('show/{id}', 'CompanyProfileController@show');
    Route::post('update', 'CompanyProfileController@update');
    Route::delete('delete/{id}', 'CompanyProfileController@delete'); 
    Route::get('vacancy', 'VacancyController@index_for_company');
    
});


Route::group([
    'middleware' => 'api',
    'prefix' => 'vacancy'
], function ($router) {
    Route::get('home', 'VacancyController@index_for_student');
    Route::post('store', 'VacancyController@store');
    Route::get('formhelper', 'VacancyController@formHelper');
    Route::get('show/{id}', 'VacancyController@show');
    Route::post('update/{id}', 'VacancyController@update');
    Route::delete('delete/{id}', 'VacancyController@delete'); 
    
});




Route::group([
    'middleware' => 'api',
    'prefix' => 'application'
], function ($router) {
    Route::get('home', 'ApplicationController@index');
    Route::post('store', 'ApplicationController@store');
    Route::get('show/{id}', 'ApplicationController@show');
    Route::post('cancel', 'ApplicationController@cancelVacancy');
    Route::post('accept', 'ApplicationController@acceptVacancy');
    Route::post('reject', 'ApplicationController@rejectVacancy');
    Route::get('downloadcv/{application_id}', 'ApplicationController@downloadCv');
    Route::get('downloadappletter/{application_id}', 'ApplicationController@downloadAppLetter');
    Route::post('callformeeting', 'ApplicationController@callForMeeting'); 
    Route::delete('delete/{id}', 'ApplicationController@delete'); 
    
});