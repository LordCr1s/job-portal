<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language');
            $table->enum('speaking', ['Fluent', 'Very Good', 'Good', 'Confident', 'Poor']);
            $table->enum('writting', ['Fluent', 'Very Good', 'Good', 'Confident', 'Poor']);
            $table->enum('reading', ['Fluent', 'Very Good', 'Good', 'Confident', 'Poor']);
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
